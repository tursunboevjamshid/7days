<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function(){
	return redirect('/'.\App::getLocale());
});


Route::group([
    'prefix' => '{lang}',
    'where' => ['lang' => '(uz|ru|oz)'],
    'middleware' => 'locale'
], function() {

	
Route::get('/', 'HomeController@home');
Route::get('post/{post}', 'PostController@show');
Route::get('office/{office}', 'HomeController@ticketoffice')->name('offices');
Route::get('tickets', 'HomeController@tickets');
Route::get('cars', 'HomeController@cars');
Route::get('albums', 'HomeController@albums');
Route::get('ticketoffices', 'HomeController@ticketoffices');
Route::post('test', 'HomeController@test');
Route::get('country/{slug}', 'HomeController@country');
Route::get('country/{slug}/albums', 'HomeController@countryAlbums');
Route::get('category/{slug}', 'HomeController@category');
Route::get('album/{slug}', 'HomeController@album');
Route::post('office/{office}/comment', 'HomeController@officecomment');
Route::get('travels', 'HomeController@travels');
Route::get('gids', 'HomeController@gids');
Route::get('gid/{id}', 'HomeController@gid');
Route::get('offices/{type}', 'HomeController@offices');
Route::get('author/{user}', 'HomeController@author');

Route::get('/home', 'HomeController@index')->name('home');    

});

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('smm/{id}', 'AdminController@smm');
    Route::get('custom/post/{id}', 'AdminController@post');
});
Auth::routes();

Route::get('/setlocale/{lang}', function($lang){
	$langs = ['ru', 'uz'];
	if(in_array($lang, $langs)){
		session(['language' => $lang]);
	}
	return redirect('/');
});
