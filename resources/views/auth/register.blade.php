{{-- @extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection --}}

@extends('layouts.master')

@section('content')
<div class="cabinetregistr">

        <div class="container">
            <form method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
                    
            <div class="row">
                <div class="col-md-12">
                    <div class="saytkirish">
                        <span>Ro’yxatdan o’tish</span>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="registration">
                        <form action="#">
                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="login">FIO: </label>
                                <input type="text" name="name" id="login" value="{{ old('name') }}">
                            </div>
                            <div class="form-group{{ $errors->has('datebirth') ? ' has-error' : '' }}">
                                <label for="datebirth">Tug’ilgan sanasi: </label>
                                <input type="text" id="datebirth" name="datebirth" value="">
                            </div>
                            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email">Elektron pochtangiz:</label>
                                <input type="email" id="email" name="email" value="{{ old('email') }}">
                            </div>

                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="telnumber">Telefon raqamingiz: </label>
                                <input type="tel" id="telnumber" name="phone" value="">
                            </div>
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                <label for="telnumber">Login: </label>
                                <input type="tel" id="telnumber" name="username" value="{{ old('username') }}">
                                @if ($errors->has('username'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="passwordregis">Parolni kiriting: </label>
                                <input type="password" id="passwordregis" name="password" value="">
                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label for="passwordregis">Parolni qaytadan kiriting: </label>
                                    <input type="password" id="passwordregis" name="password_confirmation" value="">
                                </div>
                    </div>

                </div>
            </div>
                <div class="row">
                    <div class="royhatdanotish">
                        <div class="col-md-6 col-md-offset-3">
                            <label for="saqlabqolish">
                                <input type="checkbox" name="confirmation" value="1" id="saqlabqolish"> Konfedensiallik yo’riqnomasi va portal
                                qoidalariga roziman
                            </label>
                            @if ($errors->has('confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="royhatdanotish">
                        <div class="col-md-6 col-md-offset-3">
                            <a href="#" onclick="document.querySelector('form').submit();">Ro’yxatdan o’tish</a>
                        </div>
                    </div>
                </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>
        </form>
        </div>
    </div>
    @foreach($errors->all() as $error)
        {{ $error }}
    @endforeach
@endsection