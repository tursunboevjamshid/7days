@extends('layouts.master')

@section('content')
<div class="body">
    <div class="container">

        <div class="row">
            <div class="col-md-9">
                <div class="carusell">
                    <div class="icon">
                        <a href="#"> <img src="boostin/images/fb.svg" alt=""></a>
                        <a href="#"><img src="boostin/images/tg.svg" alt=""></a>
                        <a href="#"> <img src="boostin/images/Insta.svg" alt=""></a>
                        <a href="#"> <img src="boostin/images/twitter.svg" alt=""></a>
                        <a href="#"> <img src="boostin/images/odno.svg" alt=""></a>
                    </div>
                    <div class="righttext">
                        <ul>
                            @foreach($featured as $post)
                            <li><a href="/{{ App::getLocale() }}/post/{{ $post->slug }}">{{ $post->title }}</a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="rigthreklama"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="addreklama"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9 newspanelrigth">
                <div class="news">
                    <span>Yangiliklar</span>
                    <a href="/{{ App::getLocale() }}/category/yangiliklar" class="barmaqol">Barcha maqolalar <i class=""></i></a>
                </div>
                @foreach($latestPosts as $post)
                    @include('layouts.post')
                @endforeach

            </div>

            @include('layouts.sidebar')
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="sayohuchunhead">
                    <span>Sayohat uchun eng yaxshi yo’nalishlar</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="sayohuchunbody">
                @foreach($countries as $country)
                <div class="col-md-3">
                    <a href="/{{ App::getLocale() }}/country/{{ $country->slug }}" class="fortravel">
                        <img src="{{  Voyager::image($country->cropped) }}" alt="">
                        <span>{{ $country->name }}</span>
                    </a>
                </div>
                @endforeach
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="addreklama"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="bilishfoyd">
                    <span>Bilish foydali</span>
                </div>
            </div>
        </div>

        @include('layouts.useful')

    </div>
</div>
@endsection