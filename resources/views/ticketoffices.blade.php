@extends('layouts.master')

@section('content')

<div class="bodysayohatlar">
        <div class="izlash">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="endyahwi">Eng yaxshi sayohatlarni bepul topib berish</h3>
                    </div>
                </div>
                <div class="row">
                    <div class="inputgroup">
                        <div class="col-md-2 col-md-offset-1">
                            <label for="mamlaket">Mamlakat</label>
                            <input type="text" placeholder="Turkey" id="mamlaket">
                        </div>
                        <div class="col-md-2">
                            <label for="data">Borish sanasi</label>
                            <input type="date" placeholder="Turkey" id="data">
                        </div>
                        <div class="col-md-2">
                            <label for="muddat">Muddat</label>
                            <input type="text" placeholder="2 kub" id="muddat">
                        </div>
                        <div class="col-md-2">
                            <label for="turistlar">Turistlar</label>
                            <input type="text" placeholder="Turistlar" id="turistlar">
                        </div>
                        <div class="col-md-2">
                            <label for="turistlar">Turistlar</label>
                            <input type="text" placeholder="Turistlar" id="turistlar">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-9 newspanelrigth">

                    <div class="agentliklar">
                        <span>Aviakassalar</span>
                        <a href="/{{ App::getLocale() }}/ticketoffices" class="barmaqol">Barcha agentliklar<i class=""></i></a>
                    </div>

                    @foreach($offices as $office)
                    <div class="leftcard">
                        <div class="card">
                            <ul>
                            <li class="companyname"><a href="/{{ App::getLocale() }}/office/{{$office->id}}">{{ $office->name }}</a></li>
                                <li class="mchj">{{ $office->company }}</li>
                                <li class="manzil">Manzil: {{ $office->address }}</li>
                                <li class="telefon">Telefon: <a href="#">{{ $office->phone }}</a></li>
                            <li class="sana"><a href="http://{{ $office->website }}">{{ $office->website }}</a>
                                    <ul>
                                        <li><a href="#">{{ $office->email }}</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    <a href="{{ App::getLocale() }}/office/{{$office->id}}"><img src="/storage/{{ $office->logo }}" alt=""></a>
                    </div>
                    @endforeach
                    {!! $offices->links() !!}
                </div>
              

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>


        </div>

    </div>

@endsection