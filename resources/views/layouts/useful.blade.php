 <div class="row">
                <div class="usefulltonow">
                    @foreach($useful as $post)
                    <div class="col-md-3">
                        <a href="/{{ App::getLocale() }}/post/{{ $post->slug }}" class="card">
                            <img src="/storage/{{ $post->image }}" alt="">
                            <div class="usefull">{{ $post->title }}
                            </div>
                        </a>
                    </div>
                    @endforeach
                    <div class="clearfix"></div>
                </div>
                <div class="yanamaqolachiqar">
                    <a href="/{{ App::getLocale() }}/category/{{ $post->category->slug }}" class="yanamaqol">Yana maqolalar</a>
                </div>
            </div>