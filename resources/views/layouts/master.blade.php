<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>@yield('title')</title>
    <link rel="stylesheet" href="/style/font-awesome.min.css">
    <link rel="stylesheet" href="/style/bootsrap.css">
    <link rel="stylesheet" href="/style/style.css">
    @yield('header')
</head>
<body>
<div class="wrapper">

    <!--header-->
    @include('layouts.header')
    <!--Body-->
    @yield('content')
    <!--footer-->
    <footer>
    <div class="container-fluid">
        <div class="footertop">

        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12 footerow">
                    <img src="boostin/images/logo-footer.svg" alt="">
                    <img class="pull-right" src="boostin/images/collect.png" alt="">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <ul class="leftsaytmat">
                        <li> Sayt materiallaridan foydalanganda 7Days.uz manbaa sifatida ko’rsatilishi shart!
                        </li>
                        <li> Saytda xatolikni ko’rgan bo’lsangiz, Ctrl + Enter tugmalari ( macOs tizimida ⌘ + Enter
                            ) orqali ma’muriyatga xatoni yuboring
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="saytreklama">
                        <li><a href="#">Saytda reklama</a></li>
                        <li><a href="#">Foydalanish shartlari</a></li>
                        <li><a href="#">Konfedensiallik yo'riqnomasi</a></li>
                        <li><a href="#">Biz bilan bog'lanish</a></li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="instambatfoter">
                        <li>
                            <a href="#">
                                <img src="boostin/images/rectangle-12-copy-3.jpg" alt="">
                            </a>
                            <a href="#">
                                <img src="boostin/images/rectangle-12-copy-3.jpg" alt="">
                            </a>
                            <a href="#">
                                <img src="boostin/images/rectangle-12-copy-3.jpg" alt="">
                            </a>
                            <a href="#">
                                <img src="boostin/images/rectangle-12-copy-3.jpg" alt="">
                            </a>
                            <a href="#">
                                <img src="boostin/images/rectangle-12-copy-3.jpg" alt="">
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="boostin/images/rectangle-12-copy-6.jpg" alt="">
                            </a>
                            <a href="#">
                                <img src="boostin/images/rectangle-12-copy-6.jpg" alt="">
                            </a>
                            <a href="#">
                                <img src="boostin/images/rectangle-12-copy-6.jpg" alt="">
                            </a>
                            <a href="#">
                                <img src="boostin/images/rectangle-12-copy-6.jpg" alt="">
                            </a>
                            <a href="#"><img src="boostin/images/rectangle-12-copy-6.jpg" alt=""></a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="copywrite">
            <div class="row">
                <div class="col-md-9">
                    <p>&copy; 7Days.uz - Barcha huquqlar himoyalangan <a href="#">(OAV sifatida №12312 raqami bilan
                        ro’yxatga olingan)</a></p>
                </div>
                <div class="col-md-3">
                    <p class="pull-right boost">Powered by <a href="#">Boostin Media</a></p></div>
            </div>

        </div>
    </div>
</footer>

</div>
<script src="/boostin/js/jquery-3.2.1.min.js"></script>
<script src="/boostin/js/bootstrap.min.js"></script>
@yield('scripts')
</body>
</html>