<header>
        <div class="container">

            <div class="row">
                <div class="col-md-4 maqola">
                    <a href="/admin" class="maqola_joylash">Maqola joylash</a>
                </div>
                <div class="col-md-4">
                    <div class="right_border"></div>
                    <a href="/"><img src="/boostin/images/logo.svg" alt=""></a>
                    <div class="left_border"></div>
                </div>
                <div class="col-md-4 account">
                    <div class="left_acount">
                        @auth
                            <img src="/boostin/images/ic-account-circle-black-24-px.svg" class="icon_account" alt="">
                            <span data-toggle="dropdown">{{ Auth::user()->username }}</span>
                        @endauth
                        @guest
                            <span><a href="/login">Kirish</a></span>
                        @endguest

                            <ul class="dropdown-menu">
                                <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Logout
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                    </div>
                </div>
            </div>



            <div class="row">
                <div class="col-md-12">
                    <div class="nav_bar">
                        <ul>
                            <li><a href="/{{ App::getLocale() }}/travels">Sayohat</a></li>
                            <li><a href="/{{ App::getLocale() }}/tickets">Aviabilet</a></li>
                            <li><a href="#">Mehmonxona</a></li>
                            <li><a href="/{{ App::getLocale() }}/cars">Ijaraga avto</a></li>
                            <li><a href="/{{ App::getLocale() }}/gids">Ekskursiya</a></li>
                            <li><a href="/{{ App::getLocale() }}/category/fikr">Fikr</a></li>
                            <li><a href="/{{ App::getLocale() }}/albums">Fotogalereya</a></li>
                            <li><a href="/{{ App::getLocale() }}/category/yangiliklar">Yangiliklar</a></li>
                            <li><a href="#">Market</a></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </header>