<div class="leftcard">
    <div class="card">
        <ul>
            <li class="turizmi">
                <a href="/{{ App::getLocale() }}/post/{{ $post->slug }}">
                    {{ $post->title }}
                </a>
            </li>
            <li class="ilgari">
                {{ $post->excerpt }}
            </li>
            <li class="sevdays"><a href="#">{{ $post->author->name }}</a></li>
            <li class="sana"><a href="#">{{ $post->created_at }}</a>
                <ul>
                    <li>o’qish uchun {{ $post->readingTime }} daqiqa</li>
                </ul>
            </li>
        </ul>
    </div>
    <a href="/{{ App::getLocale() }}/post/{{ $post->slug }}">
        <img src="{{  Voyager::image($post->cropped) }}" alt="">
    </a>
</div>