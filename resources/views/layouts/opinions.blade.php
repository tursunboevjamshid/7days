<div class="row">
                <div class="usefulltonow">
                    @foreach($opinions as $opinion)
                        <div class="col-md-3">
                            <a href="/{{ App::getLocale() }}/post/{{ $opinion->slug }}" class="card">
                                <img src="/storage/{{ $opinion->image }}" alt="">
                                <div class="usefull">
                                    {{ $opinion->title }}
                                </div>
                            </a>
                        </div>
                    @endforeach
                    <div class="clearfix"></div>
                </div>
                <div class="yanamaqolachiqar">
                    <a href="/{{ App::getLocale() }}/category/fikr" class="yanamaqol">Yana maqolalar</a>
                </div>
            </div>
