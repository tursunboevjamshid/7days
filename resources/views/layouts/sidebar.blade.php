
<div class="col-md-3">
    <div class="fikrlar">
        <span>Fikrlar</span>
    </div>
    <div class="rigthcard">
        @foreach($opinions as $opinion)
            <div class="card">
                <ul>
                    <li class="fikrlenta"><a href="/{{ App::getLocale() }}/post/{{ $opinion->title }}">
                        {{ $opinion->title }}
                    </a></li>
                    <li class="sevdays"><a href="/{{ App::getLocale() }}/office/{{$opinion->author->id}}">{{ $opinion->author->name }}</a>
                        <ul class="underlist">
                            <li>{{ $post->views }}</li>
                        </ul>
                    </li>
                </ul>
            </div>
        @endforeach
    </div>
    <!-- <div class="savoljavoblar">
        <span>Savol-javoblar</span>
    </div>
    <div class="cardcardsavoljavob">
        @foreach($questions as $question)
        <div class="card">
            <ul>
                <li><a href="/post/{{ $question->slug }}">
                    {{ $question->title }}
                </a></li>
                <li class="numebanswers"><a href="#">Javob berilgan</a>
                    <ul>
                        <li>3123 ta javob</li>
                    </ul>
                </li>
            </ul>
        </div>
        @endforeach
    </div> -->

</div>