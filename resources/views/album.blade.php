@extends('layouts.master')

@section('content')
    <!--body-->
    <div class="bodypost commentsbody"><!-- Faqat shu div da .commentsbody boladi. *Faqat shu sahifada -->

      <div class="container">
        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="panelhaed">
              <ul class="user">
                <li class="usercicrle">
                  <a href="#">
                                </a>
                  <img src="boostin/images/oval-2-copy.png" alt="">
                </li>
                <li class="usermalumot"><a href="#">{{ $album->author->name }} <br>
                            </a>{{ $album->created_at }} - Fotogalereya <br>O’qish uchun {{ $album->readingTime }} daqiqa<br>
                </li>
              </ul>

            </div>
            <div class="turizmnirivoj">
              <h1>{{ $album->title }}</h1>
              <p>{{ $album->excerpt }}
              </p>
            </div>
            <!-- Rasmni o'zi .thisphoto class bilan qo'yilishi kerak. chtob men uni topishim uchun -->
            <img src="/storage/{{ $album->image }}" alt="Photo" class="photo thisphoto">

          </div>
        </div>
      </div>


      <div class="container">

        <div class="row">
          <div class="col-md-8 col-md-offset-2">
            <div class="textturizm">
              {!! $album->body !!}
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="addreklama"></div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <div class="turistfikri">
              <span>Turistlarning fikri</span>
            </div>
          </div>
        </div>

        @include('layouts.opinions')

      </div>

    </div>
@endsection
  @section('scripts')
  <script src="/boostin/js/7days.js"></script>
  <script src="/boostin/js/comments.js"></script> <!-- Shu js faqat shu sahifada qo'shilishi kerak -->
  @endsection