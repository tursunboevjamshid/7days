@extends('layouts.master')
@section('title'){{ $post->title }}@endsection
@section('content')
<div class="body">
    <div class="bodypost">

        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panelhaed">
                        <ul class="user">
                            <li class="usercicrle">
                                <a href="#">
                                </a>
                                <img src="boostin/images/oval-2-copy.png" alt="">
                            </li>
                            <li class="usermalumot"><a href="/{{ App::getLocale() }}/author/{{ $post->author->id }}">{{ $post->author->name }} <br>
                            </a>{{ $post->created_at->diffForHumans() }} - {{ $post->category->getTranslatedAttribute('name') }}<br>O’qish uchun {{ $post->readingTime }} daqiqa<br>
                            </li>

                            
                        </ul>

                    </div>
                    <div class="turizmnirivoj">
                        <h1>{{ $post->getTranslatedAttribute('title') }}</h1>
                        <p>{!! $post->getTranslatedAttribute('excerpt') !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>

        <div class="imgbodyp">
            <img src="/storage/{{ $post->image }}" alt="">
        </div>

        <div class="container">

            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="textturizm">
                        {!! $post->getTranslatedAttribute('body') !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="turistfikri">
                        <span>Turistlarning fikri</span>
                    </div>
                </div>
            </div>

            @include('layouts.opinions')

        </div>

    </div>
</div>
@endsection