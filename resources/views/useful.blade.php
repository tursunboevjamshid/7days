@extends('layouts.master')

@section('content')
    <!--Body-->
    <div class="body">

        <div class="container">

            

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9 newspanelrigth">
                    <div class="news">
                        <span>Fikrlar</span><!-- Maqolalar ro'yxati bo'lishi mumkin -->
                        
                    </div>
                @foreach($posts as $post)
                    <div class="leftcard">
                        <div class="card">
                            <ul>
                                <li class="turizmi">
                                    <a href="/post/{{ $post->slug }}">
                                        {{ $post->title }}
                                    </a>
                                </li>
                                <li class="ilgari">
                                    {{ $post->excerpt }}
                                </li>
                                <li class="sevdays"><a href="#">{{ $post->author->name }}</a></li>
                                <li class="sana"><a href="#">{{ $post->created_at }}</a>
                                    <ul>
                                        <li>o’qish uchun {{ $post->readingTime }} daqiqa</li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <a href="{{ $post->slug }}"><img src="{{  Voyager::image($post->cropped }}" alt=""></a>
                    </div>
                @endforeach

                </div>

                @include('layouts.sidebar')
            </div>


     

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="bilishfoyd">
                        <span>Bilish foydali</span>
                    </div>
                </div>
            </div>

            @include('layouts.useful')

        </div>

    </div>
@endsection