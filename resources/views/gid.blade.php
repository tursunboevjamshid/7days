@extends('layouts.master')

@section('content')
    <!--Body-->
<div class="gidsingle">

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="giduser">
                    <div class="media">
                        <div class="media-left">
                            <img src="/storage/{{ $gid->user->avatar }}" class="media-object" style="width:60px">
                        </div>
                        <div class="media-body">
                            <h4 class="media-heading">{{ $gid->name }}</h4>
                            <p>{{ $gid->country->name }}dagi gid</p>
                            <div class="hayotgozal">
                                <span>{{ $gid->slogan }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="sayohuchunhead">
                    <span>Gid haqida ma’lumotlar</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="gidhaqida">
                <div class="col-md-4">
                    <ul>
                        <li class="telfon">Telefon: <a href="#">{{ $gid->phone }}</a></li>
                        <li class="pochta">Elektron pochta: <a href="#">{{ $gid->email }}</a></li>
                        <li class="vebsayt">Veb-sayt: <a href="#">{{ $gid->website }}</a></li>
                        <li class="facebook">Facebook: <a href="#">{{ $gid->facebook }}</a></li>
                        <li class="nick">Telegram: <a href="#">{{ $gid->telegram }}</a></li>
                        <li class="twitter">Twitter: <a href="#">{{ $gid->twitter }}</a></li>
                        <li class="ok">Ok.ru: <a href="#">{{ $gid->ok }}</a></li>
                        <li class="vk">VK.com: <a href="#">{{ $gid->vk }}</a></li>
                        <li class="instagram">Instagram: <a href="#">{{ $gid->instagram }}</a></li>
                        <li class="ulanish">Ulashish:</li>
                        <li class="share">
                            <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
                            <script src="//yastatic.net/share2/share.js"></script>
                            <div class="ya-share2"
                                 data-services="vkontakte,facebook,odnoklassniki,twitter,linkedin,telegram"
                                 data-counter=""></div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <div class="gidlarhaqidatext">
                        {!! $gid->info !!}
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="addreklama"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="bilishfoyd">
                    <span>Gidning ekskursiyalari</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="usefulltonow">
                @foreach($tours as $tour)
                <div class="col-md-3">
                    <a href="/{{ App::getLocale() }}/post/{{ $tour->slug }}" class="card">
                        <img src="{{ Voyager::image($tour->cropped) }}" alt="">
                        <div class="usefull">{{ $tour->title }}
                        </div>
                    </a>
                </div>
                @endforeach
                <div class="clearfix"></div>
            </div>
            <div class="yanamaqolachiqar">
                <a href="/{{ App::getLocale() }}/category/sayohat?author={{ $gid->user->id }}" class="yanamaqol">Yana maqolalar</a>
            </div>
        </div>

    </div>

</div>
@endsection