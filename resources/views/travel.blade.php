@extends('layouts.master')

@section('content')
    <div class="bodysayohatlar">
        <div class="qidiruv">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="endyahwi">Eng yaxshi sayohatlarni bepul topib berish</h3>
                    </div>
                </div>
                <div class="row">
                  <div class="inputgroup">
                      <div class="col-md-3">
                          <label for="mamlaket">Mamlakat</label>
                          <input type="text" placeholder="Turkey" id="mamlaket">
                      </div>
                      <div class="col-md-3">
                          <label for="data">Borish sanasi</label>
                          <input type="date" placeholder="Turkey" id="data">
                      </div>
                      <div class="col-md-3">
                          <label for="muddat">Muddat</label>
                          <input type="text" placeholder="2 kub" id="muddat">
                      </div>
                      <div class="col-md-3">
                          <label for="turistlar">Turistlar</label>
                          <input type="text" placeholder="Turistlar" id="turistlar">
                      </div>
                  </div>
                </div>
                <div class="row">
                  <div class="inputgroup">
                      <div class="col-md-3">
                          <label for="telepon">Telefon raqamingiz</label>
                          <input type="tel" placeholder="+998 90 805 59 95" id="telepon">
                      </div>
                      <div class="col-md-3">
                          <label for="name">Ismingiz</label>
                          <input type="text" placeholder="Ism Familiya" id="name">
                      </div>
                      <div class="col-md-3">
                          <label for="bilet">Bilet ham sotib olasizmi?</label>
                          <input type="text" placeholder="Ha" id="bilet">
                      </div>
                      <div class="col-md-3">
                          <a href="#" class="sayohatbutton">
                              Sayohat qilish</a>
                      </div>
                  </div>
                </div>
            </div>
        </div>

        <div class="container">

            <div class="row">

                <div class="col-md-9 newspanelrigth">
                    <div class="sayohatlar">
                        <span>Sayohatlar</span>
                        <a href="/{{ App::getLocale() }}/category/sayohat" class="barmaqol">Barcha maqolalar <i class=""></i></a>
                    </div>
                    @foreach($posts as $post)
                      @include('layouts.post')
                    @endforeach

                    <div class="agentliklar">
                        <span>Agentliklar</span>
                        <a href="/{{ App::getLocale() }}/offices/tour" class="barmaqol">Barcha agentliklar<i class=""></i></a>
                    </div>

                    @foreach($offices as $office)
                    <div class="leftcard">
                        <div class="card">
                            <ul>
                            <li class="companyname"><a href="/{{ App::getLocale() }}/office/{{$office->id}}">{{ $office->name }}</a></li>
                                <li class="mchj">{{ $office->company }}</li>
                                <li class="manzil">Manzil: {{ $office->address }}</li>
                                <li class="telefon">Telefon: <a href="#">{{ $office->phone }}</a></li>
                            <li class="sana"><a href="http://{{ $office->website }}">{{ $office->website }}</a>
                                    <ul>
                                        <li><a href="#">{{ $office->email }}</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    <a href="{{ App::getLocale() }}/office/{{$office->id}}"><img src="/storage/{{ $office->logo }}" alt=""></a>
                    </div>
                    @endforeach

                   <div class="paginationbar">
                       
                   </div>
                </div>

                @include('layouts.sidebar')

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>

            <div class="scroll">

                <div class="row">
                    <div class="col-md-12">
                        <div class="mamlakat">
                            <span>Mamlakatlar bo’yicha sayohatlar</span>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="scrolcards">
                            <div class="scrollhead vizasiz">
                                <span>Vizasiz borish mumkin</span>
                            </div>
                            <div class="scrollmenu">
                                @foreach($novisas as $tour)
                                <a href="/{{ App::getLocale() }}/country/{{ $tour->country->slug }}"
                                   style="background-image: url('{{ Voyager::image($tour->cropped) }}')">
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="scrolcards">
                            <div class="scrollhead vizabilan">
                                <span>Faqat Viza bilan</span>
                            </div>
                            <div class="scrollmenu">
                                @foreach($onlyvisas as $tour)
                                <a href="/{{ App::getLocale() }}/country/{{ $tour->country->slug }}"
                                   style="background-image: url('{{ Voyager::image($tour->cropped) }}')">
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="scrolcards">
                            <div class="scrollhead borganviza">
                                <span>Borgandan so’ng Viza qilinadi</span>
                            </div>
                            <div class="scrollmenu">
                                @foreach($aftervisas as $tour)
                                <a href="/{{ App::getLocale() }}/country/{{ $tour->country->slug }}"
                                   style="background-image: url('{{ Voyager::image($tour->cropped) }}')">
                                </a>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
@endsection