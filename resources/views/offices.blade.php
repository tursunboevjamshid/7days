@extends('layouts.master')

@section('content')

<div class="bodysayohatlar">

        <div class="container">

            <div class="row">

                <div class="col-md-9 newspanelrigth">

                    <div class="agentliklar">
                        <span>{{ $type }}</span>
                    </div>

                    @foreach($offices->translate() as $office)
                    <div class="leftcard">
                        <div class="card">
                            <ul>
                            <li class="companyname"><a href="/{{ App::getLocale() }}/office/{{$office->id}}">{{ $office->name }}</a></li>
                                <li class="mchj">{{ $office->company }}</li>
                                <li class="manzil">Manzil: {{ $office->address }}</li>
                                <li class="telefon">Telefon: <a href="#">{{ $office->phone }}</a></li>
                            <li class="sana"><a href="http://{{ $office->website }}">{{ $office->website }}</a>
                                    <ul>
                                        <li><a href="#">{{ $office->email }}</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    <a href="#"><img src="/storage/{{ $office->logo }}" alt=""></a>
                    </div>
                    @endforeach
                    {!! $offices->links() !!}
                </div>
              

            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>


        </div>

    </div>

@endsection