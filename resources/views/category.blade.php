@extends('layouts.master')

@section('content')
    <!--Body-->
    <div class="body">

        <div class="container">

            

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-9 newspanelrigth">
                    <div class="news">
                        <span>{{ $category->translate()->name }}</span><!-- Maqolalar ro'yxati bo'lishi mumkin -->
                        
                    </div>
                @foreach($posts->translate() as $post)
                    @include('layouts.post')
                @endforeach

                {!! $posts->links() !!}

                </div>

                @include('layouts.sidebar')
            </div>


     

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="bilishfoyd">
                        <span>Bilish foydali</span>
                    </div>
                </div>
            </div>

            @include('layouts.useful')

        </div>

    </div>
@endsection