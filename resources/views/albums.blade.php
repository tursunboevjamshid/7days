@extends('layouts.master')

@section('content')

    <div class="fotoalbom">

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="bilishfoyd">
                        <span>Eng yaxshi albomlar</span>
                    </div>
                </div>
            </div>
            @foreach($top->chunk(4) as $albums)
            <div class="row">
                <div class="albomlar">
                    @foreach($albums as $album)
                    <div class="col-md-3">
                        <a href="/{{ App::getLocale() }}/album/{{ $album->slug }}" class="card"
                           style="background-image: url('/storage/{{ $album->image  }}')">
                            <span class="username">{{ $album->author->name }}</span>
                            <span class="view"><img src="boostin/images/fotoalbomlar/eye.svg" alt="">{{ $album->views }}</span>
                            <p>{{ $album->title }}</p>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach


            <div class="row">
                <div class="col-md-12">
                    <div class="bilishfoyd">
                        <span>So’nggi albomlar</span>
                    </div>
                </div>
            </div>

            @foreach($latest->chunk(4) as $albums)
            <div class="row">
                <div class="albomlar">
                    @foreach($albums as $album)
                    <div class="col-md-3">
                        <a href="/{{ App::getLocale() }}/album/{{ $album->slug }}" class="card"
                           style="background-image: url('/storage/{{ $album->image  }}')">
                            <span class="username">{{ $album->author->name }}</span>
                            <span class="view"><img src="boostin/images/fotoalbomlar/eye.svg" alt="">{{ $album->views }}</span>
                            <p>{{ $album->title }}</p>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            @endforeach

            <div class="row">
                <div class="col-md-12">
                    <div class="bilishfoyd">
                        <span>Davlatlar bo’yicha</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="table">
                    <div class="col-md-2">
                        <ul>
                            @foreach($countries as $country)
                            <li><a href="/{{ App::getLocale() }}/country/{{ $country->slug }}/albums">
                                <img src="/storage/{{ $country->flag }}" alt="">
                                <span>{{ $country->name }}</span></a></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="bilishfoyd">
                        <span>Bilish foydali</span>
                    </div>
                </div>
            </div>
           @include('layouts.useful')
        </div>

    </div>
    @endsection