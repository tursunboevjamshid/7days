@extends('layouts.master')

@section('content')
<div class="body">

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="addreklama"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-9 rentcardlist">

                <div class="news">
                    <span>Avtoyangiliklar</span>
                    <a href="/{{ App::getLocale() }}/category/avto-yangiliklar" class="barmaqol">Barcha maqolalar <i class=""></i></a>
                </div>
                @foreach($posts as $post)
                    @include('layouts.post')
                @endforeach

                <div class="avtoijara">

                    <div class="news">
                        <span>Avto ijalar firmalar katalogi</span>
                        <a href="/{{ App::getLocale() }}/offices/avto" class="barmaqol">Barcha maqolalar <i class=""></i></a>
                    </div>

                    @foreach($offices as $office)
                        <div class="leftcard">
                            <div class="card">
                                <ul>
                                    <li class="companyname">
                                        <a href="/{{ App::getLocale() }}/office/{{ $office->id }}">
                                            {{ $office->name }}
                                        </a>
                                    </li>
                                    <li class="mchj">{{ $office->company }}</li>
                                    <li class="manzil">Manzil: {{ $office->address }}</li>
                                    <li class="telefon">Telefon: <a href="#">{{ $office->phone }}</a></li>
                                <li class="sana"><a href="http://{{ $office->website }}">{{ $office->website }}</a>
                                        <ul>
                                            <li><a href="#">{{ $office->email }}</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                        <a href="#"><img src="/storage/{{ $office->logo }}" alt=""></a>
                        </div>
                    @endforeach

                </div>

                <!--<div class="tavsiyaqilamiz">
                    <div class="news"><span>Tavsiya qilamiz</span></div>
                    <div class="leftcard">
                        <div class="card">
                            <ul>
                                <li class="companyname"><a href="#">Aviatime</a></li>
                                <li class="tavsiyamatn">
                                    <p>Tashkilotchilarning ma'lumotlariga ko'ra, mazkur yarmarkada 250 dan ortiq
                                        tashkilot va sayyohlik kompaniyalari 1353ta stendda o‘z xizmatlarini
                                        namoyish etdi. Uch kun mobaynida yarmarkaga 106 mingdan ortiq kishi tashrif
                                        buyurdi.</p>
                                </li>
                            </ul>
                        </div>
                        <a href="#"><img src="boostin/images/country/main-logoregistered.png" alt=""></a>
                    </div>
                    <div class="leftcard">
                        <div class="card">
                            <ul>
                                <li class="companyname"><a href="#">Arzon aviabiletlar</a></li>
                                <li class="tavsiyamatn">
                                    <p>Tashkilotchilarning ma'lumotlariga ko'ra, mazkur yarmarkada 250 dan ortiq
                                        tashkilot va sayyohlik kompaniyalari 1353ta stendda o‘z xizmatlarini
                                        namoyish etdi. Uch kun mobaynida yarmarkaga 106 mingdan ortiq kishi tashrif
                                        buyurdi.</p>
                                </li>
                            </ul>
                        </div>
                        <a href="#"><img src="boostin/images/country/main-logoregistered.png" alt=""></a>
                    </div>
                    <div class="leftcard">
                        <div class="card">
                            <ul>
                                <li class="companyname"><a href="#">TezTour</a></li>
                                <li class="tavsiyamatn">
                                    <p>Tashkilotchilarning ma'lumotlariga ko'ra, mazkur yarmarkada 250 dan ortiq
                                        tashkilot va sayyohlik kompaniyalari 1353ta stendda o‘z xizmatlarini
                                        namoyish etdi. Uch kun mobaynida yarmarkaga 106 mingdan ortiq kishi tashrif
                                        buyurdi.</p>
                                </li>
                            </ul>
                        </div>
                        <a href="#"><img src="boostin/images/country/main-logoregistered.png" alt=""></a>
                    </div>

                </div> -->

            </div>

                @include('layouts.sidebar')
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="addreklama"></div>
            </div>
        </div>

    </div>

</div>
@endsection