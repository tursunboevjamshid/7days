@extends('layouts.master')

@section('content')
<div class="bodygidlar">

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <span>Eng yaxshi ekskursiyalar</span>
                    </div>
                </div>
            </div>

            <div class="row">
                @foreach($gids as $gid)
                <div class="col-md-3">
                    <div class="gidlarbox">
                        <div class="boxheader">
                            <img src="boostin/images/gidlar/rectangle-3.png" alt="">
                            <div class="imgbefore">
                                <img src="/storage/{{ $gid->user->avatar }}" alt="">
                            </div>
                            <p>{{ $gid->name }}</p>
                            <span>{{ $gid->country->name }}dagi gid</span>
                        </div>
                        <div class="gidlinks">
                            <a href="/{{ App::getLocale() }}/travels?gid={{ $gid->user->id }}" class="gidlist">
                                <ul>
                                    <li><img src="boostin/images/gidlar/suitcase.png" alt=""></li>
                                    <li>Sayohatlari</li>
                                </ul>
                            </a>
                            <a href="/{{ App::getLocale() }}/gid/{{ $gid->id }}">
                                <ul>
                                    <li><img src="boostin/images/gidlar/warning.png" alt=""></li>
                                    <li>Anketa</li>
                                </ul>
                            </a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <span>Davlatlar bo’yicha</span>
                    </div>
                </div>
            </div>

            <div class="row">
              <div class="table">
                  <div class="col-md-2">
                      <ul>
                        @foreach($countries as $country)
                          <li><a href="/{{ App::getLocale() }}/country/{{ $country->slug }}">
                              <img src="/storage/{{ $country->flag }}" alt="">
                              <span>{{ $country->name }}</span></a></li>
                        @endforeach
                      </ul>
                  </div>
              </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="addreklama"></div>
                </div>
            </div>

        </div>

    </div>
@endsection