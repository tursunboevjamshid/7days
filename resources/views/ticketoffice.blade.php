@extends('layouts.master')

@section('content')
<div class="tourgency">

    <div class="container">

        <div class="row">
            <div class="col-md-12">
                <div class="aviatime">
                    <span>AVIATIME</span>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="selfiebody">
                <div class="col-md-4">
                    <ul>
                        <li class="selfielogo"><img
                                    src="/storage/{{ $office->logo }}" alt=""></li>
                            <li class="idora"><b>Idora: </b>{{ $office->company }} </li>
                            <li class="manzil"><b>Manzil: </b> {{ $office->address }} </li>
                            <li class="telfon">Telefon: <a href="#">{{ $office->phone }}</a></li>
                            <li class="email">E-mail: <a href="#">{{ $office->email }} </a></li>
                            <li class="sayt">Sayt: <a href="http://{{ $office->website }}">{{ $office->website }}</a></li>
                            <li class="lacation">   <div id="map" style="height:300px;"></div>                    </li>
                    </ul>
                </div>
                <div class="col-md-8">
                    <div class="bigimg">
                        <img src="/storage/{{ $office->pictures[0] }}" alt="">
                        <ul>
                            @foreach($office->pictures as $picture)
                                <li><a href="#"><img src="/storage/{{ $picture }}" alt=""></a></li>
                            @endforeach
                        </ul>
                    </div>
                    <div class="mulohazalar">
                        <span>Mijozlar fikri</span>
                        <a href="#" class="barmaqol">Barcha fikrlar <i class="fa fa-angle-right" aria-hidden="true"></i></a>
                    </div>
                    @foreach($comments->chunk(2) as $comments)
                    <div class="mijozlarfikri">
                        <div class="mijozlarcard">
                            <div class="media">
                                <div class="media-left">
                                    <img src="/storage/{{ $comments->first()->user->avatar }}" width="64px" class="media-object">
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">{{ $comments->first()->user->name }}<span> {{ $comments->first()->created_at }} </span></h4>
                                    <p> {{ $comments->first()->body }} </p>
                                </div>
                            </div>
                        </div>
                        @if($comments->first()->id != $comments->last()->id)
                        <div class="mijozlarcard right">
                            <div class="media-left">
                                <img src="/storage/{{ $comments->last()->user->avatar }}" width="64px" class="media-object">
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">{{ $comments->last()->user->name }}<span> {{ $comments->last()->created_at }} </span></h4>
                                <p> {{ $comments->last()->body }} </p>
                            </div>
                        </div>
                        @endif
                    </div>
                    @endforeach        
                    @auth
                        <div class="maqola fikrbildir">
                            <a href="javascript:void(0)" class="maqola_joylash">Fikr bildirish</a>
                        </div>
                        <div class="fikr hidden">
                          <form action="/{{ App::getLocale() }}/office/{{ $office->id }}/comment" method="post">
                            {!! csrf_field() !!}
                            <textarea name="body" rows="8" cols="1"></textarea>
                            <input type="submit" value="Fikr bildirish" class="submitbutton">
                          </form>

                        </div>
                    @endauth
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="addreklama"></div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="bilishfoyd">
                    <span>Bilish foydali</span>
                </div>
            </div>
        </div>

        @include('layouts.useful')
    </div>
</div>
@endsection



@section('scripts')
<script src="/boostin/js/7days.js"></script>
<script>

        function initMap() {
          var myLatLng = {lat: {{ $office->coordinates[0] }}, lng: {{ $office->coordinates[1] }}};
  
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 12,
            center: myLatLng
          });
  
          var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            title: '{{ $office->name }}'
          });
        }
      </script>
      <script async defer
        src="https://maps.googleapis.com/maps/api/js?key={{ env('GOOGLE_MAPS_KEY') }}&callback=initMap">
      </script>
  
@endsection