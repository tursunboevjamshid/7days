@extends('layouts.master')

@section('content')
<div class="cabinetauth">

    <div class="container">
        <form method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-12">
                    <div class="saytkirish">
                        <span>Saytga kirish</span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="saytkirishform">
                            <div class="form-group{{ $errors->has('username') ? ' has-error' : '' }}">
                                    <label for="username" class="col-md-4 control-label">Login</label>
        
                                    <div class="col-md-6">
                                        <input id="username" type="username" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
        
                                        @if ($errors->has('username'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('username') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
        
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="col-md-4 control-label">Parol</label>
        
                                    <div class="col-md-6">
                                        <input id="password" type="password" class="form-control" name="password" required>
        
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="saytkirishlink">
                    <div class="col-md-3 col-md-offset-3">
                            <label for="saqlabqolish">
                                    <input type="checkbox" id="saqlabqolish" name="remember" {{ old('remember') ? 'checked' : '' }}> Saqlab qolish
                            </label>
                    </div>
                    <div class="col-md-3">
                        <a href="#">Saytga kirish</a>
                    </div>
                </div>
            </div>
        </form>
    </div>

</div>
@endsection