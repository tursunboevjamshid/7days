@extends('layouts.master')

@section('content')
<div class="bodycountry">
    <div class="imgcountry">
        <div class="container"><h1>{{ $country->name }}</h1></div>
    </div>
    <div class="container">

        <div class="tailhaqida">
            <span>{{ $country->name }} haqida</span>
        </div>

        <div class="row">

            <div class="col-md-4">
                <ul class="leftlist">
                    <li><b>Poytaxti</b> <a href="#">{{ $country->capital }}</a></li>
                    <li><b>Rasmiy tili </b><a href="#">{{ $country->language }}</a></li>
                    <li><b>Eslatmalar!</b> {{ $country->caution }}
                    </li>
                    <li><b>Pul birligi </b>{{ $country->currency }}</li>
                    <li><b>Vaqti </b>{{ $country->timezone }}</li>
                    <li><b>Uchib borish vaqti </b>{{ $country->flight }}</li>
                    <li><b>Klimati </b>{{ $country->climate }}</li>
                    <li><b>Viza </b>{{ $country->viza }}</li>
                </ul>
            </div>
            <div class="col-md-8">
                <div class="rigthtext">
                    {!! $country->info !!}
                </div>
            </div>

        </div>

        <div class="row">
        <div class="sayohatbody">
            <div class="col-md-8">
                <div class="sayohattext">
                    <span>{{ $country->name }}ga Sayohatlar</span>
                    <a href="#">Barcha sayohatlar</a>

                </div>
                <div class="row">
                    <div class="col-md-6">
                        @foreach($country->tours->slice(0, 2) as $tour)
                        <a href="#" class="sayohatcard" 
                        style='background-image: url("/storage/{{ $tour->image  }}")'>
                            <h4>{{ $tour->name }}</h4>
                            <span>{{ $tour->price }}</span>
                        </a>
                        @endforeach
                    </div>
                    <div class="col-md-6">
                        @foreach($country->tours->slice(2, 2) as $tour)
                        <a href="#" class="sayohatcard" 
                        style='background-image: url("/storage/{{ $tour->image  }}")'>
                            <h4>{{ $tour->name }}</h4>
                            <span>{{ $tour->price }}</span>
                        </a>
                        @endforeach
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="addreklama">
                </div>
            </div>
        </div>
        </div>


        <!--<div class="row">-->
        <!--<div class="col-md-12">-->
        <!--<div class="addreklama"></div>-->
        <!--</div>-->
        <!--</div>-->


    </div>
</div>
@endsection