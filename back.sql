-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: 7days
-- ------------------------------------------------------
-- Server version	5.7.21-1ubuntu1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `albums`
--

DROP TABLE IF EXISTS `albums`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'PENDING',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `views` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `albums`
--

LOCK TABLES `albums` WRITE;
/*!40000 ALTER TABLE `albums` DISABLE KEYS */;
INSERT INTO `albums` VALUES (2,0,'New album','<p>sdffsd</p>',NULL,'new-album',0,'PENDING','2018-04-25 13:30:57','2018-04-25 13:36:28',0),(3,0,'Another album','<p>sdfsdfs</p>','albums/April2018/VaFAVOfVuOwD9waNn1Oh.jpg','russkij',0,'PENDING','2018-04-26 01:58:45','2018-04-26 02:04:13',0),(4,1,'asdsadasd','<p>asdasd</p>','albums/April2018/udnuZwwQPsL6MFb88Xss.jpg','asdsadasd',1,'PUBLISHED','2018-04-26 02:07:46','2018-04-26 02:08:26',0),(5,2,'sfdsfs','<p>sdfsfsfd</p>','albums/April2018/AWZmVPIb2UNxKxH3sYAO.jpg','sfdsfs',1,'pending','2018-04-26 02:08:53','2018-04-26 02:08:53',0),(6,1,'sfdsfs','<p>sdfsfsfd</p>','albums/April2018/UsH94rgdeqtEGjGcz7Da.jpg','sfdsfs',1,'DRAFT','2018-04-26 02:09:03','2018-04-26 02:13:27',0),(7,2,'sdsdad','<p>asdasdas</p>','albums/April2018/5w2JqdzqpnMwlVamNSGS.jpg','sdsdad',1,'PENDING','2018-04-26 02:14:08','2018-04-26 02:14:08',0);
/*!40000 ALTER TABLE `albums` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`),
  KEY `categories_parent_id_foreign` (`parent_id`),
  CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,NULL,1,'Sayohat','sayohat','2018-04-10 13:05:35','2018-04-25 11:00:16'),(2,NULL,1,'Aviakompaniya yangiliklari','avianews','2018-04-10 13:05:35','2018-04-25 11:00:41'),(3,NULL,1,'Avto yangiliklar','avto-yangiliklar','2018-04-25 11:01:31','2018-04-25 11:01:31'),(4,NULL,1,'Ekskursiyalar','ekskursiyalar','2018-04-25 11:01:44','2018-04-25 11:01:44'),(5,NULL,1,'Fikr','fikr','2018-04-25 11:01:51','2018-04-25 11:01:51'),(6,NULL,1,'Yangiliklar','yangiliklar','2018-04-25 11:01:57','2018-04-25 11:01:57'),(7,NULL,1,'Savol-javoblar','savol-javoblar','2018-04-25 11:02:21','2018-04-25 11:02:21'),(8,NULL,1,'Bilish foydali','bilish-foydali','2018-04-25 11:03:22','2018-04-25 11:03:22');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `picture` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `capital` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `language` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `caution` text COLLATE utf8_unicode_ci,
  `currency` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `timezone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `climate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `flight` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `viza` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'countries/April2018/WlphO3JyveOO7Wz6cR7u.png','Bangkok','Tay','Hech qachon monarx va buddani haqoratlamang; suv yoki o’simlik teringizni shikastlaganini sezsangiz darhol shifokorga murojaat qiling, ba’zi o’simliklar zaxarli hisoblanadi.','bat','+7','Tropik','5 soat','Shart emas','<p>Ekzotik va bugun ommabop ommaviy joylardan biri bo\'lgan Tailand, sayyohlarga ajoyib sharqona sayohlarni, Janubiy-Sharqiy Osiyoning eng boy tabiati va buddist madaniyatining ajoyib merosini taklif etadi. Bundan tashqari, Tailandda yuqori mavsum Rossiyaning birinchi qor bilan qoplanganidan boshlanadi, shuning uchun agar qish mavsumi ekzotik bilan birga yozmoqchi bo\'lsangiz, bu yerda siz to\'g\'ri yo\'lga borasiz. Mamlakatdagi o\'yin-kulgi har qanday lazzatlanish uchun va \"Men qayerga boraman\" sayyohnii savoliga deyarli ishonchli javob berish mumkin: \"Taylandga!&rdquo;</p>\r\n<p>Ekzotik va bugun ommabop ommaviy joylardan biri bo\'lgan Tailand, sayyohlarga ajoyib sharqona sayohlarni, Janubiy-Sharqiy Osiyoning eng boy tabiati va buddist madaniyatining ajoyib merosini taklif etadi.</p>','Tailand','2018-04-12 02:39:00','2018-04-12 02:41:49','tailand'),(2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'Singapur','2018-04-26 02:58:54','2018-04-26 02:58:54','singapur');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_rows`
--

DROP TABLE IF EXISTS `data_rows`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_rows` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `data_type_id` int(10) unsigned NOT NULL,
  `field` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT '0',
  `browse` tinyint(1) NOT NULL DEFAULT '1',
  `read` tinyint(1) NOT NULL DEFAULT '1',
  `edit` tinyint(1) NOT NULL DEFAULT '1',
  `add` tinyint(1) NOT NULL DEFAULT '1',
  `delete` tinyint(1) NOT NULL DEFAULT '1',
  `details` text COLLATE utf8mb4_unicode_ci,
  `order` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `data_rows_data_type_id_foreign` (`data_type_id`),
  CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=160 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_rows`
--

LOCK TABLES `data_rows` WRITE;
/*!40000 ALTER TABLE `data_rows` DISABLE KEYS */;
INSERT INTO `data_rows` VALUES (1,1,'id','number','ID',1,0,0,0,0,0,NULL,1),(2,1,'author_id','text','Author',1,0,1,1,0,1,NULL,2),(3,1,'category_id','text','Category',0,0,1,1,1,0,NULL,3),(4,1,'title','text','Title',1,1,1,1,1,1,NULL,4),(5,1,'excerpt','text_area','Excerpt',0,0,1,1,1,1,NULL,5),(6,1,'body','rich_text_box','Body',1,0,1,1,1,1,NULL,6),(7,1,'image','image','Post Image',0,1,1,1,1,1,'{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"221\",\"height\":\"173\"}}]}',7),(8,1,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}',8),(9,1,'meta_description','text_area','Meta Description',0,0,1,1,1,1,NULL,9),(10,1,'meta_keywords','text_area','Meta Keywords',0,0,1,1,1,1,NULL,10),(11,1,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"waiting\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',11),(12,1,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,12),(13,1,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,13),(14,2,'id','number','ID',1,0,0,0,0,0,'',1),(15,2,'author_id','text','Author',1,0,0,0,0,0,'',2),(16,2,'title','text','Title',1,1,1,1,1,1,'',3),(17,2,'excerpt','text_area','Excerpt',1,0,1,1,1,1,'',4),(18,2,'body','rich_text_box','Body',1,0,1,1,1,1,'',5),(19,2,'slug','text','Slug',1,0,1,1,1,1,'{\"slugify\":{\"origin\":\"title\"}}',6),(20,2,'meta_description','text','Meta Description',1,0,1,1,1,1,'',7),(21,2,'meta_keywords','text','Meta Keywords',1,0,1,1,1,1,'',8),(22,2,'status','select_dropdown','Status',1,1,1,1,1,1,'{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}',9),(23,2,'created_at','timestamp','Created At',1,1,1,0,0,0,'',10),(24,2,'updated_at','timestamp','Updated At',1,0,0,0,0,0,'',11),(25,2,'image','image','Page Image',0,1,1,1,1,1,'',12),(26,3,'id','number','ID',1,0,0,0,0,0,NULL,1),(27,3,'name','text','Name',1,1,1,1,1,1,NULL,2),(28,3,'email','text','Email',1,1,1,1,1,1,NULL,3),(29,3,'password','password','Password',1,0,0,1,1,0,NULL,4),(30,3,'user_belongsto_role_relationship','relationship','Role',0,1,1,1,1,0,'{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}',10),(31,3,'remember_token','text','Remember Token',0,0,0,0,0,0,NULL,5),(32,3,'created_at','timestamp','Created At',0,1,1,0,0,0,NULL,6),(33,3,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,7),(34,3,'avatar','image','Avatar',0,1,1,1,1,1,NULL,8),(35,5,'id','number','ID',1,0,0,0,0,0,'',1),(36,5,'name','text','Name',1,1,1,1,1,1,'',2),(37,5,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),(38,5,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),(39,4,'id','number','ID',1,0,0,0,0,0,'',1),(40,4,'parent_id','select_dropdown','Parent',0,0,1,1,1,1,'{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}',2),(41,4,'order','text','Order',1,1,1,1,1,1,'{\"default\":1}',3),(42,4,'name','text','Name',1,1,1,1,1,1,'',4),(43,4,'slug','text','Slug',1,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\"}}',5),(44,4,'created_at','timestamp','Created At',0,0,1,0,0,0,'',6),(45,4,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',7),(46,6,'id','number','ID',1,0,0,0,0,0,'',1),(47,6,'name','text','Name',1,1,1,1,1,1,'',2),(48,6,'created_at','timestamp','Created At',0,0,0,0,0,0,'',3),(49,6,'updated_at','timestamp','Updated At',0,0,0,0,0,0,'',4),(50,6,'display_name','text','Display Name',1,1,1,1,1,1,'',5),(51,1,'seo_title','text','SEO Title',0,1,1,1,1,1,NULL,14),(52,1,'featured','checkbox','Featured',1,1,1,1,1,1,NULL,15),(53,3,'role_id','text','Role',0,1,1,1,1,1,NULL,9),(54,7,'id','number','Id',1,0,0,0,0,0,NULL,1),(55,7,'logo','image','Logo',0,1,1,1,1,1,NULL,3),(56,7,'company','text','Company',0,1,1,1,1,1,NULL,4),(57,7,'address','text','Address',0,0,1,1,1,1,NULL,5),(58,7,'phone','text','Phone',0,0,1,1,1,1,NULL,6),(59,7,'email','text','Email',0,0,1,1,1,1,NULL,7),(60,7,'website','text','Website',0,0,1,1,1,1,NULL,8),(61,7,'location','coordinates','Location',0,0,1,1,1,1,NULL,9),(62,7,'name','text','Name',0,1,1,1,1,1,NULL,2),(63,7,'images','multiple_images','Images',0,0,1,1,1,1,NULL,10),(64,7,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,11),(65,7,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,12),(66,7,'priority','number','Priority',0,1,1,1,1,1,NULL,13),(67,3,'extra','checkbox','Extra',0,0,1,1,1,1,NULL,10),(69,3,'username','text','Username',1,1,1,1,1,1,NULL,11),(70,8,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(71,8,'picture','image','Picture',0,1,1,1,1,1,NULL,2),(72,8,'capital','text','Capital',0,0,1,1,1,1,NULL,3),(73,8,'language','text','Language',0,0,1,1,1,1,NULL,4),(74,8,'caution','text_area','Caution',0,0,1,1,1,1,NULL,5),(75,8,'currency','text','Currency',0,0,1,1,1,1,NULL,6),(76,8,'timezone','text','Timezone',0,0,1,1,1,1,NULL,7),(77,8,'climate','text','Climate',0,0,1,1,1,1,NULL,8),(78,8,'flight','text','Uchish vaqti',0,0,1,1,1,1,NULL,9),(79,8,'viza','text','Viza',0,0,1,1,1,1,NULL,10),(80,8,'info','rich_text_box','Info',0,0,1,1,1,1,NULL,11),(81,8,'name','text','Name',0,1,1,1,1,1,NULL,12),(82,8,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,13),(83,8,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,14),(84,9,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(85,9,'picture','image','Picture',0,1,1,1,1,1,NULL,3),(86,9,'capital','text','Capital',0,0,1,1,1,1,NULL,4),(87,9,'language','text','Language',0,0,1,1,1,1,NULL,5),(88,9,'caution','text_area','Caution',0,0,1,1,1,1,NULL,6),(89,9,'currency','text','Currency',0,0,1,1,1,1,NULL,7),(90,9,'timezone','text','Timezone',0,0,1,1,1,1,NULL,8),(91,9,'climate','text','Climate',0,0,1,1,1,1,NULL,9),(92,9,'flight','text','Flight',0,0,1,1,1,1,NULL,10),(93,9,'viza','text','Viza',0,0,1,1,1,1,NULL,11),(94,9,'info','rich_text_box','Info',0,0,1,1,1,1,NULL,12),(95,9,'name','text','Name',0,1,1,1,1,1,NULL,2),(96,9,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,13),(97,9,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,14),(98,9,'slug','text','Slug',0,1,1,1,1,1,'{\"slugify\":{\"origin\":\"name\",\"forceUpdate\":true}}',15),(99,10,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(100,10,'name','text','Name',0,1,1,1,1,1,NULL,2),(101,10,'price','text','Price',0,1,1,1,1,1,NULL,3),(102,10,'duration','text','Duration',0,1,1,1,1,1,NULL,4),(103,10,'start','date','Start',0,1,1,1,1,1,NULL,5),(104,10,'end','date','End',0,1,1,1,1,1,NULL,6),(105,10,'image','image','Image',0,1,1,1,1,1,NULL,7),(106,10,'info','rich_text_box','Info',0,0,1,1,1,1,NULL,8),(107,10,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,9),(108,10,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,10),(110,10,'ticket_office_id','select_dropdown','Ticket Office Id',0,1,1,0,0,1,NULL,12),(111,10,'country_id','checkbox','Country Id',0,1,1,0,0,1,NULL,11),(116,10,'tour_belongsto_country_relationship_1','relationship','countries',0,1,1,1,1,1,'{\"model\":\"App\\\\Country\",\"table\":\"countries\",\"type\":\"belongsTo\",\"column\":\"id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}',16),(117,10,'tour_belongsto_ticket_office_relationship','relationship','ticket_offices',0,1,1,1,1,1,'{\"model\":\"App\\\\TicketOffice\",\"table\":\"ticket_offices\",\"type\":\"belongsTo\",\"column\":\"ticket_office_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"categories\",\"pivot\":\"0\"}',17),(118,11,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(119,11,'author_id','checkbox','Author Id',0,1,1,1,1,1,NULL,2),(120,11,'title','text','Title',0,1,1,1,1,1,NULL,3),(121,11,'body','rich_text_box','Body',0,0,1,1,1,1,NULL,4),(122,11,'image','image','Image',0,1,1,1,1,1,NULL,5),(123,11,'slug','text','Slug',0,1,1,1,1,1,'{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true}}',6),(124,11,'country_id','checkbox','Country Id',0,1,1,1,1,1,NULL,7),(125,11,'status','select_dropdown','Status',0,0,1,1,1,1,'{\"default\":\"pending\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}',8),(126,11,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,9),(127,11,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,10),(128,11,'views','number','Views',0,1,1,0,0,1,NULL,11),(129,11,'album_belongsto_user_relationship','relationship','User',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"author_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"albums\",\"pivot\":\"0\"}',12),(130,11,'album_belongsto_country_relationship','relationship','Country',0,1,1,1,1,1,'{\"model\":\"App\\\\Country\",\"table\":\"countries\",\"type\":\"belongsTo\",\"column\":\"country_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"albums\",\"pivot\":\"0\"}',13),(131,12,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(132,12,'photo','text','Photo',0,1,1,1,1,1,NULL,2),(133,12,'comment','text_area','Comment',0,1,1,1,1,1,NULL,3),(134,12,'user_id','checkbox','User Id',0,1,1,1,1,1,NULL,4),(135,12,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,5),(136,12,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,6),(137,12,'status','checkbox','Status',0,1,1,1,1,1,NULL,7),(138,12,'photo_comment_belongsto_user_relationship','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"albums\",\"pivot\":\"0\"}',8),(139,13,'id','checkbox','Id',1,0,0,0,0,0,NULL,1),(140,13,'name','text','Name',0,1,1,1,1,1,NULL,2),(141,13,'country_id','select_dropdown','Country Id',0,1,1,1,1,1,NULL,3),(142,13,'phone','text','Phone',0,0,1,1,1,1,NULL,4),(143,13,'email','text','Email',0,0,1,1,1,1,NULL,5),(144,13,'website','text','Website',0,0,1,1,1,1,NULL,6),(145,13,'facebook','text','Facebook',0,0,1,1,1,1,NULL,7),(146,13,'telegram','text','Telegram',0,0,1,1,1,1,NULL,8),(147,13,'twitter','text','Twitter',0,0,1,1,1,1,NULL,9),(148,13,'ok','text','Ok',0,0,1,1,1,1,NULL,10),(149,13,'vk','text','Vk',0,0,1,1,1,1,NULL,11),(150,13,'instagram','text','Instagram',0,0,1,1,1,1,NULL,12),(151,13,'info','rich_text_box','Info',0,0,1,1,1,1,NULL,13),(152,13,'user_id','select_dropdown','User Id',0,0,0,1,1,0,NULL,14),(154,13,'gid_belongsto_country_relationship','relationship','Country',0,1,1,1,1,1,'{\"model\":\"App\\\\Country\",\"table\":\"countries\",\"type\":\"belongsTo\",\"column\":\"country_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"albums\",\"pivot\":\"0\"}',16),(155,7,'type','select_dropdown','Type',0,1,1,1,1,1,'{\"default\":\"ticket\",\"options\":{\"TICKET\":\"ticket\",\"HOTEL\":\"hotel\",\"AUTO\":\"auto\",\"TOUR\":\"tour\"}}',14),(156,13,'created_at','timestamp','Created At',0,1,1,1,0,1,NULL,15),(157,13,'updated_at','timestamp','Updated At',0,0,0,0,0,0,NULL,16),(158,13,'gid_belongsto_user_relationship','relationship','users',0,1,1,1,1,1,'{\"model\":\"App\\\\User\",\"table\":\"users\",\"type\":\"belongsTo\",\"column\":\"user_id\",\"key\":\"id\",\"label\":\"name\",\"pivot_table\":\"albums\",\"pivot\":\"0\"}',17),(159,13,'slogan','text','Slogan',0,1,1,1,1,1,NULL,17);
/*!40000 ALTER TABLE `data_rows` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `data_types`
--

DROP TABLE IF EXISTS `data_types`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `data_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT '0',
  `server_side` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `data_types_name_unique` (`name`),
  UNIQUE KEY `data_types_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `data_types`
--

LOCK TABLES `data_types` WRITE;
/*!40000 ALTER TABLE `data_types` DISABLE KEYS */;
INSERT INTO `data_types` VALUES (1,'posts','posts','Post','Posts','voyager-news','TCG\\Voyager\\Models\\Post','TCG\\Voyager\\Policies\\PostPolicy',NULL,NULL,1,0,'2018-04-10 13:05:24','2018-04-25 12:25:50'),(2,'pages','pages','Page','Pages','voyager-file-text','TCG\\Voyager\\Models\\Page',NULL,'','',1,0,'2018-04-10 13:05:24','2018-04-10 13:05:24'),(3,'users','users','User','Users','voyager-person','TCG\\Voyager\\Models\\User','TCG\\Voyager\\Policies\\UserPolicy',NULL,NULL,1,0,'2018-04-10 13:05:24','2018-04-11 08:28:57'),(4,'categories','categories','Category','Categories','voyager-categories','TCG\\Voyager\\Models\\Category',NULL,'','',1,0,'2018-04-10 13:05:24','2018-04-10 13:05:24'),(5,'menus','menus','Menu','Menus','voyager-list','TCG\\Voyager\\Models\\Menu',NULL,'','',1,0,'2018-04-10 13:05:24','2018-04-10 13:05:24'),(6,'roles','roles','Role','Roles','voyager-lock','TCG\\Voyager\\Models\\Role',NULL,'','',1,0,'2018-04-10 13:05:24','2018-04-10 13:05:24'),(7,'ticket_offices','ticket-offices','Ticket Office','Ticket Offices',NULL,'App\\TicketOffice',NULL,NULL,NULL,1,0,'2018-04-11 04:12:39','2018-04-11 04:12:39'),(8,'Country','country','Country','Countries',NULL,'App\\Country',NULL,NULL,NULL,1,0,'2018-04-12 02:23:55','2018-04-12 02:23:55'),(9,'countries','countries','Country','Countries',NULL,'App\\Country',NULL,NULL,NULL,1,0,'2018-04-12 02:28:27','2018-04-12 02:28:27'),(10,'tours','tours','Tour','Tours',NULL,'App\\Tour',NULL,NULL,NULL,1,0,'2018-04-25 11:11:20','2018-04-25 11:11:20'),(11,'albums','albums','Album','Albums',NULL,'App\\Album',NULL,NULL,NULL,1,0,'2018-04-25 11:44:27','2018-04-25 11:44:27'),(12,'photo_comments','photo-comments','Photo Comment','Photo Comments',NULL,'App\\PhotoComment',NULL,NULL,NULL,1,0,'2018-04-25 11:51:02','2018-04-25 11:51:02'),(13,'gids','gids','Gid','Gids',NULL,'App\\Gid',NULL,NULL,NULL,1,0,'2018-04-25 11:58:53','2018-04-25 11:58:53');
/*!40000 ALTER TABLE `data_types` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gids`
--

DROP TABLE IF EXISTS `gids`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gids` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telegram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ok` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vk` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slogan` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gids`
--

LOCK TABLES `gids` WRITE;
/*!40000 ALTER TABLE `gids` DISABLE KEYS */;
INSERT INTO `gids` VALUES (2,'Gid me',2,'+6564',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'<p>asdsadda</p>',1,'2018-04-26 02:43:00','2018-04-26 03:06:08',NULL),(3,'New gid',2,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,3,'2018-04-26 02:53:00','2018-04-26 03:07:20',NULL);
/*!40000 ALTER TABLE `gids` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_items`
--

DROP TABLE IF EXISTS `menu_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_items` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu_id` int(10) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `menu_items_menu_id_foreign` (`menu_id`),
  CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `menus` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_items`
--

LOCK TABLES `menu_items` WRITE;
/*!40000 ALTER TABLE `menu_items` DISABLE KEYS */;
INSERT INTO `menu_items` VALUES (1,1,'Dashboard','','_self','voyager-boat',NULL,NULL,1,'2018-04-10 13:05:26','2018-04-10 13:05:26','voyager.dashboard',NULL),(2,1,'Media','','_self','voyager-images',NULL,NULL,5,'2018-04-10 13:05:26','2018-04-10 13:05:26','voyager.media.index',NULL),(3,1,'Posts','','_self','voyager-news',NULL,NULL,6,'2018-04-10 13:05:26','2018-04-10 13:05:26','voyager.posts.index',NULL),(4,1,'Users','','_self','voyager-person',NULL,NULL,3,'2018-04-10 13:05:27','2018-04-10 13:05:27','voyager.users.index',NULL),(5,1,'Categories','','_self','voyager-categories',NULL,NULL,8,'2018-04-10 13:05:27','2018-04-10 13:05:27','voyager.categories.index',NULL),(6,1,'Pages','','_self','voyager-file-text',NULL,NULL,7,'2018-04-10 13:05:27','2018-04-10 13:05:27','voyager.pages.index',NULL),(7,1,'Roles','','_self','voyager-lock',NULL,NULL,2,'2018-04-10 13:05:27','2018-04-10 13:05:27','voyager.roles.index',NULL),(8,1,'Tools','','_self','voyager-tools',NULL,NULL,9,'2018-04-10 13:05:27','2018-04-10 13:05:27',NULL,NULL),(9,1,'Menu Builder','','_self','voyager-list',NULL,8,10,'2018-04-10 13:05:27','2018-04-10 13:05:27','voyager.menus.index',NULL),(10,1,'Database','','_self','voyager-data',NULL,8,11,'2018-04-10 13:05:27','2018-04-10 13:05:27','voyager.database.index',NULL),(11,1,'Compass','','_self','voyager-compass',NULL,8,12,'2018-04-10 13:05:27','2018-04-10 13:05:27','voyager.compass.index',NULL),(12,1,'Settings','','_self','voyager-settings',NULL,NULL,14,'2018-04-10 13:05:27','2018-04-10 13:05:27','voyager.settings.index',NULL),(13,1,'Hooks','','_self','voyager-hook',NULL,8,13,'2018-04-10 13:05:36','2018-04-10 13:05:36','voyager.hooks',NULL),(14,1,'Ticket Offices','/admin/ticket-offices','_self','voyager-company','#000000',NULL,15,'2018-04-11 04:12:40','2018-04-26 02:24:18',NULL,''),(16,1,'Countries','/admin/countries','_self','voyager-world','#000000',NULL,17,'2018-04-12 02:28:28','2018-04-26 02:25:58',NULL,''),(17,1,'Tours','/admin/tours','_self','voyager-ticket','#000000',NULL,18,'2018-04-25 11:11:20','2018-04-26 02:27:10',NULL,''),(18,1,'Albums','/admin/albums','_self','voyager-photo','#000000',NULL,19,'2018-04-25 11:44:27','2018-04-26 02:28:09',NULL,''),(19,1,'Photo Comments','/admin/photo-comments','_self','voyager-chat','#000000',NULL,20,'2018-04-25 11:51:02','2018-04-26 02:30:02',NULL,''),(20,1,'Gids','/admin/gids','_self','voyager-people','#000000',NULL,21,'2018-04-25 11:58:53','2018-04-26 02:29:28',NULL,'');
/*!40000 ALTER TABLE `menu_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `menus_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'admin','2018-04-10 13:05:26','2018-04-10 13:05:26');
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_10_12_000000_create_users_table',1),(2,'2014_10_12_100000_create_password_resets_table',1),(3,'2016_01_01_000000_add_voyager_user_fields',1),(4,'2016_01_01_000000_create_data_types_table',1),(5,'2016_01_01_000000_create_pages_table',1),(6,'2016_01_01_000000_create_posts_table',1),(7,'2016_02_15_204651_create_categories_table',1),(8,'2016_05_19_173453_create_menu_table',1),(9,'2016_10_21_190000_create_roles_table',1),(10,'2016_10_21_190000_create_settings_table',1),(11,'2016_11_30_135954_create_permission_table',1),(12,'2016_11_30_141208_create_permission_role_table',1),(13,'2016_12_26_201236_data_types__add__server_side',1),(14,'2017_01_13_000000_add_route_to_menu_items_table',1),(15,'2017_01_14_005015_create_translations_table',1),(16,'2017_01_15_000000_add_permission_group_id_to_permissions_table',1),(17,'2017_01_15_000000_create_permission_groups_table',1),(18,'2017_01_15_000000_make_table_name_nullable_in_permissions_table',1),(19,'2017_03_06_000000_add_controller_to_data_types_table',1),(20,'2017_04_11_000000_alter_post_nullable_fields_table',1),(21,'2017_04_21_000000_add_order_to_data_rows_table',1),(22,'2017_07_05_210000_add_policyname_to_data_types_table',1),(23,'2017_08_05_000000_add_group_to_settings_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pages`
--

DROP TABLE IF EXISTS `pages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `pages_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pages`
--

LOCK TABLES `pages` WRITE;
/*!40000 ALTER TABLE `pages` DISABLE KEYS */;
INSERT INTO `pages` VALUES (1,0,'Hello World','Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.','<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','pages/page1.jpg','hello-world','Yar Meta Description','Keyword1, Keyword2','ACTIVE','2018-04-10 13:05:35','2018-04-10 13:05:35');
/*!40000 ALTER TABLE `pages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_groups`
--

DROP TABLE IF EXISTS `permission_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permission_groups_name_unique` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_groups`
--

LOCK TABLES `permission_groups` WRITE;
/*!40000 ALTER TABLE `permission_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `permission_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permission_role` (
  `permission_id` int(10) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_permission_id_index` (`permission_id`),
  KEY `permission_role_role_id_index` (`role_id`),
  CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permission_role`
--

LOCK TABLES `permission_role` WRITE;
/*!40000 ALTER TABLE `permission_role` DISABLE KEYS */;
INSERT INTO `permission_role` VALUES (1,1),(1,2),(1,3),(2,1),(3,1),(4,1),(5,1),(5,4),(6,1),(6,4),(7,1),(7,4),(8,1),(8,4),(9,1),(9,4),(10,1),(10,4),(11,1),(11,4),(12,1),(12,4),(13,1),(13,4),(14,1),(14,4),(15,1),(16,1),(17,1),(18,1),(19,1),(20,1),(20,4),(21,1),(21,4),(22,1),(22,4),(23,1),(23,4),(24,1),(24,4),(25,1),(25,2),(25,3),(25,4),(26,1),(26,2),(26,3),(26,4),(27,1),(27,2),(27,3),(27,4),(28,1),(28,2),(28,3),(28,4),(29,1),(29,2),(29,3),(29,4),(30,1),(30,4),(31,1),(31,4),(32,1),(32,4),(33,1),(33,4),(34,1),(34,4),(35,1),(36,1),(37,1),(38,1),(39,1),(40,1),(41,1),(41,4),(42,1),(42,4),(43,1),(43,4),(44,1),(44,4),(45,1),(45,4),(46,1),(46,4),(47,1),(47,4),(48,1),(48,4),(49,1),(49,4),(50,1),(50,4),(51,1),(51,4),(52,1),(52,4),(53,1),(53,4),(54,1),(54,4),(55,1),(55,4),(56,1),(56,4),(57,1),(57,4),(58,1),(58,4),(59,1),(59,4),(60,1),(60,4),(61,1),(61,2),(61,3),(61,4),(62,1),(62,2),(62,3),(62,4),(63,1),(63,2),(63,3),(63,4),(64,1),(64,2),(64,3),(64,4),(65,1),(65,2),(65,3),(65,4),(66,1),(66,4),(67,1),(67,4),(68,1),(68,4),(69,1),(69,4),(70,1),(70,4),(71,1),(71,3),(71,4),(72,1),(72,4),(73,1),(73,3),(73,4),(74,1),(74,4),(75,1),(75,4);
/*!40000 ALTER TABLE `permission_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `permissions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `permission_group_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `permissions_key_index` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `permissions`
--

LOCK TABLES `permissions` WRITE;
/*!40000 ALTER TABLE `permissions` DISABLE KEYS */;
INSERT INTO `permissions` VALUES (1,'browse_admin',NULL,'2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(2,'browse_database',NULL,'2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(3,'browse_media',NULL,'2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(4,'browse_compass',NULL,'2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(5,'browse_menus','menus','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(6,'read_menus','menus','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(7,'edit_menus','menus','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(8,'add_menus','menus','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(9,'delete_menus','menus','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(10,'browse_pages','pages','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(11,'read_pages','pages','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(12,'edit_pages','pages','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(13,'add_pages','pages','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(14,'delete_pages','pages','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(15,'browse_roles','roles','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(16,'read_roles','roles','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(17,'edit_roles','roles','2018-04-10 13:05:27','2018-04-10 13:05:27',NULL),(18,'add_roles','roles','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(19,'delete_roles','roles','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(20,'browse_users','users','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(21,'read_users','users','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(22,'edit_users','users','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(23,'add_users','users','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(24,'delete_users','users','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(25,'browse_posts','posts','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(26,'read_posts','posts','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(27,'edit_posts','posts','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(28,'add_posts','posts','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(29,'delete_posts','posts','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(30,'browse_categories','categories','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(31,'read_categories','categories','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(32,'edit_categories','categories','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(33,'add_categories','categories','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(34,'delete_categories','categories','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(35,'browse_settings','settings','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(36,'read_settings','settings','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(37,'edit_settings','settings','2018-04-10 13:05:28','2018-04-10 13:05:28',NULL),(38,'add_settings','settings','2018-04-10 13:05:29','2018-04-10 13:05:29',NULL),(39,'delete_settings','settings','2018-04-10 13:05:30','2018-04-10 13:05:30',NULL),(40,'browse_hooks',NULL,'2018-04-10 13:05:36','2018-04-10 13:05:36',NULL),(41,'browse_ticket_offices','ticket_offices','2018-04-11 04:12:39','2018-04-11 04:12:39',NULL),(42,'read_ticket_offices','ticket_offices','2018-04-11 04:12:39','2018-04-11 04:12:39',NULL),(43,'edit_ticket_offices','ticket_offices','2018-04-11 04:12:39','2018-04-11 04:12:39',NULL),(44,'add_ticket_offices','ticket_offices','2018-04-11 04:12:39','2018-04-11 04:12:39',NULL),(45,'delete_ticket_offices','ticket_offices','2018-04-11 04:12:39','2018-04-11 04:12:39',NULL),(46,'browse_Country','Country','2018-04-12 02:23:55','2018-04-12 02:23:55',NULL),(47,'read_Country','Country','2018-04-12 02:23:55','2018-04-12 02:23:55',NULL),(48,'edit_Country','Country','2018-04-12 02:23:55','2018-04-12 02:23:55',NULL),(49,'add_Country','Country','2018-04-12 02:23:55','2018-04-12 02:23:55',NULL),(50,'delete_Country','Country','2018-04-12 02:23:55','2018-04-12 02:23:55',NULL),(51,'browse_countries','countries','2018-04-12 02:28:28','2018-04-12 02:28:28',NULL),(52,'read_countries','countries','2018-04-12 02:28:28','2018-04-12 02:28:28',NULL),(53,'edit_countries','countries','2018-04-12 02:28:28','2018-04-12 02:28:28',NULL),(54,'add_countries','countries','2018-04-12 02:28:28','2018-04-12 02:28:28',NULL),(55,'delete_countries','countries','2018-04-12 02:28:28','2018-04-12 02:28:28',NULL),(56,'browse_tours','tours','2018-04-25 11:11:20','2018-04-25 11:11:20',NULL),(57,'read_tours','tours','2018-04-25 11:11:20','2018-04-25 11:11:20',NULL),(58,'edit_tours','tours','2018-04-25 11:11:20','2018-04-25 11:11:20',NULL),(59,'add_tours','tours','2018-04-25 11:11:20','2018-04-25 11:11:20',NULL),(60,'delete_tours','tours','2018-04-25 11:11:20','2018-04-25 11:11:20',NULL),(61,'browse_albums','albums','2018-04-25 11:44:27','2018-04-25 11:44:27',NULL),(62,'read_albums','albums','2018-04-25 11:44:27','2018-04-25 11:44:27',NULL),(63,'edit_albums','albums','2018-04-25 11:44:27','2018-04-25 11:44:27',NULL),(64,'add_albums','albums','2018-04-25 11:44:27','2018-04-25 11:44:27',NULL),(65,'delete_albums','albums','2018-04-25 11:44:27','2018-04-25 11:44:27',NULL),(66,'browse_photo_comments','photo_comments','2018-04-25 11:51:02','2018-04-25 11:51:02',NULL),(67,'read_photo_comments','photo_comments','2018-04-25 11:51:02','2018-04-25 11:51:02',NULL),(68,'edit_photo_comments','photo_comments','2018-04-25 11:51:02','2018-04-25 11:51:02',NULL),(69,'add_photo_comments','photo_comments','2018-04-25 11:51:02','2018-04-25 11:51:02',NULL),(70,'delete_photo_comments','photo_comments','2018-04-25 11:51:02','2018-04-25 11:51:02',NULL),(71,'browse_gids','gids','2018-04-25 11:58:53','2018-04-25 11:58:53',NULL),(72,'read_gids','gids','2018-04-25 11:58:53','2018-04-25 11:58:53',NULL),(73,'edit_gids','gids','2018-04-25 11:58:53','2018-04-25 11:58:53',NULL),(74,'add_gids','gids','2018-04-25 11:58:53','2018-04-25 11:58:53',NULL),(75,'delete_gids','gids','2018-04-25 11:58:53','2018-04-25 11:58:53',NULL);
/*!40000 ALTER TABLE `permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `photo_comments`
--

DROP TABLE IF EXISTS `photo_comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `photo_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `user_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'pending',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `photo_comments`
--

LOCK TABLES `photo_comments` WRITE;
/*!40000 ALTER TABLE `photo_comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `photo_comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `posts_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,1,1,'Lorem Ipsum Post',NULL,'This is the excerpt for the Lorem Ipsum Post','<p>This is the body of the lorem ipsum post</p>','posts/post1.jpg','lorem-ipsum-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2018-04-10 13:05:35','2018-04-10 13:30:53'),(2,1,1,'My Sample Post',NULL,'This is the excerpt for the sample Post','<p>This is the body for the sample post, which includes the body.</p>\r\n<h2>We can use all kinds of format!</h2>\r\n<p>And include a bunch of other stuff.</p>','posts/post2.jpg','my-sample-post','Meta Description for sample post','keyword1, keyword2, keyword3','PUBLISHED',0,'2018-04-10 13:05:35','2018-04-10 13:31:09'),(3,1,1,'Latest Post',NULL,'This is the excerpt for the latest post','<p>This is the body for the latest post</p>','posts/post3.jpg','latest-post','This is the meta description','keyword1, keyword2, keyword3','PUBLISHED',0,'2018-04-10 13:05:35','2018-04-10 13:31:15'),(4,1,1,'Yarr Post',NULL,'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.','<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\r\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\r\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>','posts/post4.jpg','yarr-post','this be a meta descript','keyword1, keyword2, keyword3','PUBLISHED',0,'2018-04-10 13:05:35','2018-04-10 13:31:19'),(5,1,1,'New Post',NULL,'T. sohasidagi munosabatlarni huquqiy jihatdan tartibga solish, turistik xizmatlar bozorini rivojlantirish, shuningdek, turistlar va turistik faoliyat subʼyektlarining huquqlari va qonuniy manfaatlarini himoya qilish maqsadida Oʻzbekiston Respublikasining \"Turizm toʻgʻrisida\"gi qonuni kabul kilingan (1999 y. 20 avg .)','<p>Turizm (frans. tour &mdash; sayr, sayohat), sayyoqlik &mdash; sayohat (safar) qilish; faol dam olish turlaridan biri. T. deganda jismoniy shaxsning doimiy istiqomat joyidan sogʻlomlashtirish, maʼrifiy, kasbiyamaliy yoki boshqa maqsadlarda borilgan joyda (mamlakatda) haq toʻlanadigan faoliyat bilan shugʻullanmagan holda uzogʻi bilan 1 y. muddatga joʻnab ketishi (sayohat qilishi) tushuniladi.</p>\r\n<div class=\"imgbody\"><img src=\"http://localhost:8000/storage/posts/April2018/Uzbek-Palov.jpg\" alt=\"\" width=\"1024\" height=\"682\" /><img src=\"file:///home/jamshid/Programming/HTML/Html/boostin/images/rectangle-17.png\" alt=\"\" /></div>\r\n<div class=\"row\">\r\n<div class=\"col-md-8 col-md-offset-2\">\r\n<div class=\"textturizm\">\r\n<p>Turizm (frans. tour &mdash; sayr, sayohat), sayyoqlik &mdash; sayohat (safar) qilish; faol dam olish turlaridan biri. T. deganda jismoniy shaxsning doimiy istiqomat joyidan sogʻlomlashtirish, maʼrifiy, kasbiyamaliy yoki boshqa maqsadlarda borilgan joyda (mamlakatda) haq toʻlanadigan faoliyat bilan shugʻullanmagan holda uzogʻi bilan 1 y. muddatga joʻnab ketishi (sayohat qilishi) tushuniladi.</p>\r\n<p>Turizm (frans. tour &mdash; sayr, sayohat), sayyoqlik &mdash; sayohat (safar) qilish; faol dam olish turlaridan biri. T. deganda jismoniy shaxsning doimiy istiqomat joyidan sogʻlomlashtirish, maʼrifiy, kasbiyamaliy yoki boshqa maqsadlarda borilgan joyda (mamlakatda) haq toʻlanadigan faoliyat bilan shugʻullanmagan holda uzogʻi bilan 1 y. muddatga joʻnab ketishi (sayohat qilishi) tushuniladi.</p>\r\n<p>T.ning tarixi 19-a. boshlariga borib takaladi. Dastlab Angliyadan Franiiyaga uyushgan sayyohlik tashkil etilgan (1815 y.). T.ning asoschisi hisoblanmish ingliz ruhoniysi Tomas Kuk 1843 yilda 1temir yoʻl sayyoxligini tashkil qildi. Shundan soʻng u oʻzining xususiy turistik korxonasini tuzdi va 1866 y. dastlabki sayyoxlik guruxlari AQShga joʻnatiddi. Sharqda arab sayyohi Ibn Battuta 21 yoshida sayohatini boshlab, deyarli barcha Sharq va Shim. Afrika mamlakatlarini piyoda kezib chikdi.</p>\r\n<p>Movarounnahrda ilk sayyohlarning safarlari Amir Temur va temuriylar davrida faollashgan. Amir Temur fransuz qiroli Karl VI va ingliz qiroli Genrix IV bilan doimiy aloqada boʻlgan. Uning elchisi 1403 y. Parijga kelgan. Ispaniyalik Klavixoning \"Buyuk Temurning hayoti va faoliyati\" kitobida Movarounnahrdagi ijtimoiy hayot va sayyoxlarning Temur davlatiga intilishi aks etgan.</p>\r\n<p>Hozirgi davrda T. dunyoning juda koʻp mamlakatlarida ommaviy tuye olgan. Odatda, T. turistik tashkilotlar orqali turistik marshrutlar boʻyicha uyushtiriladi. T.ning juda koʻp turlari va shakllari mavjud (ichki, xalqaro, havaskorlik T.i, uyushgan T., yaqin joyga sayohat, uzoqqa sayohat, bilim saviyasini kengaitirish maqsadida T., toqqa chiqish, suv T.i, avtoturizm, piyoda yuriladigan T., sport T.i va boshqalar).</p>\r\n<p>Oʻzbekistonda T. sohasiga rahbarlikni \"Oʻzbekturizm\" milliy kompaniyasi (1992 y. 27 iyulda tuzilgan) olib boradi. Kompaniyaning asosiy vazifasi T. infrastrukturasini rivojlantirish, chet el sarmoyasini jalb qilib zamonaviy turistik komplekslarni barpo etish, yangi turistik yoʻnalishlarni ishlab chiqish, xizmatlar doirasini kengaitirish va boshqalardan iborat.</p>\r\n<p>\"Oʻzbekturizm\" milliy kompaniyasi sayohat qilish turiga qarab quyidagi turistik yoʻnalishlarni ishlab chiqqan: klassik yoʻnalish (Toshkent, Samarqand, Buxoro, Xiva, Toshkent; Toshkent, Samarkand, BuxoroShahrisabz, Toshkent). Bu yoʻnalish eng qad. yodgorliklar va boshqa tarixiymadaniy obidalarga tashrif bilan bogʻliq; ekologik T. yoʻnalishi (Chimyon, Chorvoq dam olish va davolanish oromgohi, Zomin qoʻriqxonasi, Buxoro viloyatidagi qoʻriqxonalar). Bu yoʻnalish alohida muhofaza qilinadigan tabiiy hududlar va sayyohlar uchun ekologik jihatdan qulay gʻamda foydali hisoblanadigan joylarga tashrif bilan bogʻliq; arxeologik T. yoʻnalishi (Qoraqalpogʻiston, Surxondaryo, Samarkand hududdari boʻylab). Bu yoʻnalish Oʻzbekistonning eng qad. topilmalari va arxeologik qazishmalar olib borilayotgan joylari bilan tanishishni maqsad qilib qoʻyadi; ekstre mal T. yoʻnalishi (Chimyon, Fargʻona vodiysi, Orol boʻyi, Buxoro, Navoiy viloyati hududlari boʻylab); liniy T. yoʻnalishi (Toshkent, Samarkand, Buxoro, Toshkent) &mdash; mamlakatimizdagi tarixiy diniy obidalarni ziyorat qilish bilan bogʻliq.</p>\r\n<p>T. sohasidagi munosabatlarni huquqiy jihatdan tartibga solish, turistik xizmatlar bozorini rivojlantirish, shuningdek, turistlar va turistik faoliyat subʼyektlarining huquqlari va qonuniy manfaatlarini himoya qilish maqsadida Oʻzbekiston Respublikasining \"Turizm toʻgʻrisida\"gi qonuni kabul kilingan (1999 y. 20 avg .). Shu bilan birga Oʻzbekiston Respublikasi Prezidentining \"2005 yilgacha boʻlgan davrda Oʻzbekistonda turizmni rivojlantirish davlat dasturi toʻgʻrisida\"gi farmoni (1999 y. 15 aprel) sohani rivojlantirishda muhim omil boʻldi.</p>\r\n<p>T. tashkilotlarini takomillashtirish hamda kichik va oʻrta turistik korxonalarning xizmat koʻrsatish bozorini faollashtirish, shuningdek, xorijiy sarmoyani T. sohasiga jalb kilish maqsadida 1998 y. Oʻzbekiston Respublikasi hukumati qarori bilan Toshkentda \"Xususiy sayyoxlik tashkilotlari uyushmasi\" tashkil etildi. U 300 dan ziyod turistik korxonalar bilan yaqindan aloqada boʻlib faoliyat olib boradi. Oʻzbekistonda \"Kumushkon\" turistik bazasi va \"Sanzar\" kemping majmuasi mavjud boʻlib, ular \"Oʻzbekturizm\" milliy kompaniyasi tizimi tashkilotlari hisoblanadi. Shu bilan birga Chorvoq, Chimyon, Beldersoy dam olish oromgohlari va yuzga yaqin xususiy mehmonxonalar ishlab turibdi. Oʻzbekistonda koʻplab turistlarni qabul qilish va ularga xizmat koʻrsatish imkoniyatiga ega boʻlgan mehmonxonalar soni tobora oʻsib bormoqda.</p>\r\n<p>Oʻzbekiston 1993 yilda oʻz safiga 120 dan ortiq mamlakatni birlashtirgan Jahon turistik tashkiloti (WTO; 1975 y. tuzilgan)ga aʼzo boʻldi. Shuningdek, Oʻzbekiston WTO Yevropa komissiyasi rayosatining ham aʼzosidir. 2004 y. \"Buyuk ipak yoʻli\" loyihasi doirasida Samarkand viloyatida Jahon turistik tashkilotining vakolatxonasini ochish koʻzda tutilgan. Oʻzbekistonda T. sohasiga oid \"Buyuk ipak yoʻli\" xalqaro turistik reklamaaxborot gaz. (1994 yildan), \"Biznes Gayd\" JUR (RUS va ingliz tillarida) va boshqa ommaviy nashrlar chop etiladi.</p>\r\n</div>\r\n</div>\r\n</div>','posts/April2018/QsCAsPMsC14AfchtLqAD.jpg','new-post',NULL,NULL,'PUBLISHED',0,'2018-04-10 13:42:26','2018-04-10 13:47:15'),(8,2,1,'Salom post!','titele mega teelele','Qisqacha','<p>Post ichki ma\'lumotlari</p>','posts/April2018/aX7rdW58q4k5xMnHTKhr.jpg','salom-post!','Seo meta description','metakey, keeey, hello','DRAFT',0,'2018-04-26 03:15:02','2018-04-26 03:15:02');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'admin','Administrator','2018-04-10 13:05:27','2018-04-10 13:05:27'),(2,'user','Normal User','2018-04-10 13:05:27','2018-04-10 13:05:27'),(3,'gid','Gid','2018-04-25 12:31:12','2018-04-25 12:31:12'),(4,'editor','Editor','2018-04-25 12:34:24','2018-04-25 12:34:24');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT '1',
  `group` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `settings_key_unique` (`key`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (1,'site.title','Site Title','Site Title','','text',1,'Site'),(2,'site.description','Site Description','Site Description','','text',2,'Site'),(3,'site.logo','Site Logo','','','image',3,'Site'),(4,'site.google_analytics_tracking_id','Google Analytics Tracking ID','','','text',4,'Site'),(5,'admin.bg_image','Admin Background Image','','','image',5,'Admin'),(6,'admin.title','Admin Title','Voyager','','text',1,'Admin'),(7,'admin.description','Admin Description','Welcome to Voyager. The Missing Admin for Laravel','','text',2,'Admin'),(8,'admin.loader','Admin Loader','','','image',3,'Admin'),(9,'admin.icon_image','Admin Icon Image','','','image',4,'Admin'),(10,'admin.google_analytics_client_id','Google Analytics Client ID (used for admin dashboard)','','','text',1,'Admin');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ticket_offices`
--

DROP TABLE IF EXISTS `ticket_offices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ticket_offices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `logo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `company` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` point DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `images` varchar(1000) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `priority` int(11) DEFAULT '0',
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'TICKET',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ticket_offices`
--

LOCK TABLES `ticket_offices` WRITE;
/*!40000 ALTER TABLE `ticket_offices` DISABLE KEYS */;
INSERT INTO `ticket_offices` VALUES (1,'ticket-offices/April2018/Sm1VOtKEKnDlwEpVEJ6Y.png','“Doni Trade And Service” MCHJ','Toshkent shahar, Muqimiy ko’chasi 4 uy','+998 90 805 59 95','selfietouruz@mail.ru','selfietour.uz','\0\0\0\0\0\0\0�\�w��D@o0R`�QQ@','AviaTime','[\"ticket-offices\\/April2018\\/aivBeqwMJyl5bxFot44o.jpg\",\"ticket-offices\\/April2018\\/a94pGcgr930MGA4JrXjd.jpg\",\"ticket-offices\\/April2018\\/J1sx1wA5Fpx8pFQU7VhJ.jpg\"]','2018-04-11 05:28:00','2018-04-12 05:07:25',1,'TICKET');
/*!40000 ALTER TABLE `ticket_offices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tours`
--

DROP TABLE IF EXISTS `tours`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tours` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `duration` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `start` date DEFAULT NULL,
  `end` date DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `info` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `ticket_office_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tours`
--

LOCK TABLES `tours` WRITE;
/*!40000 ALTER TABLE `tours` DISABLE KEYS */;
/*!40000 ALTER TABLE `tours` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `translations`
--

DROP TABLE IF EXISTS `translations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `translations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) unsigned NOT NULL,
  `locale` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `translations`
--

LOCK TABLES `translations` WRITE;
/*!40000 ALTER TABLE `translations` DISABLE KEYS */;
INSERT INTO `translations` VALUES (1,'data_types','display_name_singular',1,'pt','Post','2018-04-10 13:05:35','2018-04-10 13:05:35'),(2,'data_types','display_name_singular',2,'pt','Página','2018-04-10 13:05:35','2018-04-10 13:05:35'),(3,'data_types','display_name_singular',3,'pt','Utilizador','2018-04-10 13:05:35','2018-04-10 13:05:35'),(4,'data_types','display_name_singular',4,'pt','Categoria','2018-04-10 13:05:35','2018-04-10 13:05:35'),(5,'data_types','display_name_singular',5,'pt','Menu','2018-04-10 13:05:35','2018-04-10 13:05:35'),(6,'data_types','display_name_singular',6,'pt','Função','2018-04-10 13:05:35','2018-04-10 13:05:35'),(7,'data_types','display_name_plural',1,'pt','Posts','2018-04-10 13:05:35','2018-04-10 13:05:35'),(8,'data_types','display_name_plural',2,'pt','Páginas','2018-04-10 13:05:35','2018-04-10 13:05:35'),(9,'data_types','display_name_plural',3,'pt','Utilizadores','2018-04-10 13:05:35','2018-04-10 13:05:35'),(10,'data_types','display_name_plural',4,'pt','Categorias','2018-04-10 13:05:35','2018-04-10 13:05:35'),(11,'data_types','display_name_plural',5,'pt','Menus','2018-04-10 13:05:35','2018-04-10 13:05:35'),(12,'data_types','display_name_plural',6,'pt','Funções','2018-04-10 13:05:35','2018-04-10 13:05:35'),(13,'categories','slug',1,'pt','categoria-1','2018-04-10 13:05:35','2018-04-10 13:05:35'),(14,'categories','name',1,'pt','Categoria 1','2018-04-10 13:05:36','2018-04-10 13:05:36'),(15,'categories','slug',2,'pt','categoria-2','2018-04-10 13:05:36','2018-04-10 13:05:36'),(16,'categories','name',2,'pt','Categoria 2','2018-04-10 13:05:36','2018-04-10 13:05:36'),(17,'pages','title',1,'pt','Olá Mundo','2018-04-10 13:05:36','2018-04-10 13:05:36'),(18,'pages','slug',1,'pt','ola-mundo','2018-04-10 13:05:36','2018-04-10 13:05:36'),(19,'pages','body',1,'pt','<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>','2018-04-10 13:05:36','2018-04-10 13:05:36'),(20,'menu_items','title',1,'pt','Painel de Controle','2018-04-10 13:05:36','2018-04-10 13:05:36'),(21,'menu_items','title',2,'pt','Media','2018-04-10 13:05:36','2018-04-10 13:05:36'),(22,'menu_items','title',3,'pt','Publicações','2018-04-10 13:05:36','2018-04-10 13:05:36'),(23,'menu_items','title',4,'pt','Utilizadores','2018-04-10 13:05:36','2018-04-10 13:05:36'),(24,'menu_items','title',5,'pt','Categorias','2018-04-10 13:05:36','2018-04-10 13:05:36'),(25,'menu_items','title',6,'pt','Páginas','2018-04-10 13:05:36','2018-04-10 13:05:36'),(26,'menu_items','title',7,'pt','Funções','2018-04-10 13:05:36','2018-04-10 13:05:36'),(27,'menu_items','title',8,'pt','Ferramentas','2018-04-10 13:05:36','2018-04-10 13:05:36'),(28,'menu_items','title',9,'pt','Menus','2018-04-10 13:05:36','2018-04-10 13:05:36'),(29,'menu_items','title',10,'pt','Base de dados','2018-04-10 13:05:36','2018-04-10 13:05:36'),(30,'menu_items','title',12,'pt','Configurações','2018-04-10 13:05:36','2018-04-10 13:05:36'),(31,'albums','title',3,'ru','Русский','2018-04-26 01:58:45','2018-04-26 01:58:45'),(32,'albums','body',3,'ru','<p>ывыфвфыв</p>','2018-04-26 01:58:45','2018-04-26 01:58:45'),(33,'data_types','display_name_singular',11,'ru','Album','2018-04-26 02:03:15','2018-04-26 02:03:15'),(34,'data_types','display_name_plural',11,'ru','Albums','2018-04-26 02:03:16','2018-04-26 02:03:16'),(35,'albums','title',4,'ru','уцкцукцукцк','2018-04-26 02:07:46','2018-04-26 02:07:46'),(36,'albums','body',4,'ru','<p>asdasdasd</p>','2018-04-26 02:07:46','2018-04-26 02:07:46'),(37,'albums','title',5,'ru','','2018-04-26 02:08:53','2018-04-26 02:08:53'),(38,'albums','body',5,'ru','','2018-04-26 02:08:53','2018-04-26 02:08:53'),(39,'albums','title',6,'ru','','2018-04-26 02:09:03','2018-04-26 02:09:03'),(40,'albums','body',6,'ru','','2018-04-26 02:09:03','2018-04-26 02:09:03'),(41,'albums','title',7,'ru','','2018-04-26 02:14:08','2018-04-26 02:14:08'),(42,'albums','body',7,'ru','','2018-04-26 02:14:08','2018-04-26 02:14:08'),(43,'data_types','display_name_singular',7,'ru','Ticket Office','2018-04-26 02:18:10','2018-04-26 02:18:10'),(44,'data_types','display_name_plural',7,'ru','Ticket Offices','2018-04-26 02:18:10','2018-04-26 02:18:10'),(45,'menu_items','title',14,'ru','Ticket Offices','2018-04-26 02:24:18','2018-04-26 02:24:18'),(47,'menu_items','title',16,'ru','Countries','2018-04-26 02:25:58','2018-04-26 02:25:58'),(48,'menu_items','title',17,'ru','Tours','2018-04-26 02:27:10','2018-04-26 02:27:10'),(49,'menu_items','title',18,'ru','Albums','2018-04-26 02:28:08','2018-04-26 02:28:08'),(50,'menu_items','title',20,'ru','Gids','2018-04-26 02:29:28','2018-04-26 02:29:28'),(51,'menu_items','title',19,'ru','Photo Comments','2018-04-26 02:30:02','2018-04-26 02:30:02'),(52,'data_types','display_name_singular',13,'ru','Gid','2018-04-26 02:40:51','2018-04-26 02:40:51'),(53,'data_types','display_name_plural',13,'ru','Gids','2018-04-26 02:40:51','2018-04-26 02:40:51'),(54,'posts','title',8,'ru','In russiant','2018-04-26 03:15:02','2018-04-26 03:18:04'),(55,'posts','seo_title',8,'ru','','2018-04-26 03:15:02','2018-04-26 03:15:02'),(56,'posts','excerpt',8,'ru','','2018-04-26 03:15:02','2018-04-26 03:15:02'),(57,'posts','body',8,'ru','<p>Russian e</p>','2018-04-26 03:15:02','2018-04-26 03:18:04'),(58,'posts','slug',8,'ru','in-russiant','2018-04-26 03:15:02','2018-04-26 03:18:04'),(59,'posts','meta_description',8,'ru','','2018-04-26 03:15:02','2018-04-26 03:15:02'),(60,'posts','meta_keywords',8,'ru','','2018-04-26 03:15:02','2018-04-26 03:15:02');
/*!40000 ALTER TABLE `translations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(11) DEFAULT '2',
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT 'users/default.png',
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `extra` text COLLATE utf8mb4_unicode_ci,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_login_unique` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,1,'Admin','admin@admin.com','users/default.png','$2y$10$LhyeUEHZ0dOKPYK/SIlQkeb9lq43t/oz.l1PrFE9S.6W0iWwi2Ztm','Lo6gIZQKAp6cxvHq0cQqt2d77fLdFN2b3xrlBYWvPaQgWnkdQvXU9kRt3WUH','2018-04-10 13:05:35','2018-04-11 08:29:10','0','admin'),(2,2,'Jamshid','jamshid@mail.ru','users/default.png','$2y$10$niB0hMbgJH2F8kW/CMApJ.IYwoFZCN6M55XYpWnKCh5s8GMU9nY9W','IwZwxXXefmj2LjCGAVK2Xa7LZyFkDUQEnpWa2KIyqQPJ1nD37ZrWimz9weD7','2018-04-11 11:09:17','2018-04-25 12:28:53','1','jamshid'),(3,3,'Gid','gid@mail.ru','users/default.png','$2y$10$ojlOcpn4LAYdDGFZ5OkpyuxtFL5wARjbN8z1y7d8XJB1x2dJ.FmB6','lxkHsXmEC9MXgeVjuRDE2ektJobBTLxnRMOyFWRstQ3PhMH5BghgqOwilxFL','2018-04-26 02:31:36','2018-04-26 02:31:49','1','gid');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-04-28 17:53:17
