<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OfficeComment extends Model
{

	public function user(){
		return $this->belongsTo(User::class);
	}

	public function office(){
		return $this->belongsTo(User::class);
	}
    
}
