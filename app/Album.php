<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;

class Album extends Model
{
	use Translatable;
	protected $translatable = ['title', 'body'];

	public function save(array $options = [])
    {
        // If no author has been assigned, assign the current user's id as the author of the post
        if (!$this->author_id && \Auth::user()) {
            $this->author_id = \Auth::user()->id;
        }

        parent::save();
    }

    public function authorId()
    {
        return $this->belongsTo(Voyager::modelClass('User'), 'author_id', 'id');
    }
    
    public function author(){
        return $this->belongsTo(User::class);
    }

    public function country(){
        return $this->belongsTo(Country::class);
    }
    public function getReadingTimeAttribute(){
        return round(substr_count($this->body, " ")/275);
    }
}
