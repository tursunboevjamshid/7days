<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\HasRelationships;
use TCG\Voyager\Traits\Translatable;

class Gid extends Model
{
	use HasRelationships,
		Translatable;
	protected $translatable = ['info', 'slogan'];
    
    public function country(){
    	return $this->belongsTo(Country::class);
    }

    public function user(){
    	return $this->belongsTo(User::class);
    }
}
