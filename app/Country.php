<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Resizable;
use TCG\Voyager\Traits\Translatable;

class Country extends Model
{
    use Resizable;
    use Translatable;
    protected $translatable = ['name', 'capital', 'language', 'caution', 'climate', 'flight', 'info'];

    public function tours(){
    	return $this->hasMany(Tour::class);
   	}

   	public function albums(){
    	return $this->hasMany(Album::class);
   	}

   	    public function getCroppedAttribute()
    {
        $attribute = 'picture';
        $type = 'cropped';
        // Return empty string if the field not found
        if (!isset($this->attributes[$attribute])) {
            return '';
        }

        // We take image from posts field
        $image = $this->attributes[$attribute];

        // We need to get extension type ( .jpeg , .png ...)
        $ext = pathinfo($image, PATHINFO_EXTENSION);

        // We remove extension from file name so we can append thumbnail type
        $name = str_replace_last('.'.$ext, '', $image);

        // We merge original name + type + extension
        return $name.'-'.$type.'.'.$ext;
    }

}
