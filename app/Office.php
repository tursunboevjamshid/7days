<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Spatial;
use TCG\Voyager\Traits\Translatable;

class Office extends Model
{
    use Spatial,
        Translatable;
    protected $translatable = ['address'];

    
    protected $spatial = ['location'];
    
    public function newQuery($excludeDeleted = true)
    {
        $raw='';
        foreach($this->spatial as $column){
            $raw .= ' astext(' . $column . ') as ' . $column . ' ';
        }
        return parent::newQuery($excludeDeleted)->addSelect('*', \DB::raw($raw));
    }
    
    public function getCoordinatesAttribute()
    {
        $loc =  substr($this->location, 6);
        $loc = preg_replace('/[ ,]+/', ',', $loc, 1);
        $loc = substr($loc, 0, -1);
        $log = preg_replace("[POINT()]", "", $loc);
        return explode(',', $loc);
    }
    
    public function getPicturesAttribute(){
        return json_decode($this->images, true);
    }

    public function comments(){
        return $this->hasMany(OfficeComment::class);
    }

    public function localComments() {
        return $this->comments()->where('lang','=', \App::getLocale())->get();
    }
}
