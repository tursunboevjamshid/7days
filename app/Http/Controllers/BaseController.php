<?php
namespace App\Http\Controllers;

class BaseController extends Controller
{
  public function __construct()
  {
    //its just a dummy data object.
    $user = \Auth::user();

    // Sharing is caring
    \View::share('user', $user);
  }
}