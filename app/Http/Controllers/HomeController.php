<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Post;
use \App\User;
use \App\Office;

class HomeController extends BaseController
{
    public function home(){
        $useful = \App\Post::where('category_id', '=', '8')->orderBy('id', 'DESC')->take(3)->get()->translate();
        $latestPosts = Post::latest()->orderBy('id', 'DESC')->take(5)->get()->translate();
        $featured = Post::where('featured', '1')->take(4)->orderBy('id', 'DESC')->get()->translate();
        $countries = \App\Country::take(4)->get()->translate();
        return view('index', array_merge($this->getRightSidebar(), compact('latestPosts', 'useful', 'featured', 'countries')));
    }   
    
    public function ticketoffice($lang, Office $office){
        $comments = $office->localComments();
        $office = $office->translate();
        $useful = \App\Post::where('category_id', '=', '8')->orderBy('id', 'DESC')->take(3)->get()->translate();
        return view('ticketoffice', compact('office', 'useful', 'comments'));
    }

    public function ticketoffices(){
        $offices = \App\Office::where('type', 'TICKET')->paginate(8)->translate();
        return view('ticketoffices', compact('offices'));
    }

    public function countryAlbums($lang, $slug){
        $country = \App\Country::where('slug', $slug)->first();
        $useful = \App\Post::where('category_id', '=', '8')
                            ->orderBy('id', 'DESC')->take(3)
                            ->get()->translate();
        $top = $country->albums()->orderBy('views', 'desc')->take(8)->get()->translate();
        $latest = $country->albums()->take(8)->orderBy('id', 'DESC')->get()->translate();
        $countries = \App\Country::all()->translate();
        return view('albums', compact('top', 'latest', 'useful', 'countries'));      
    }
    
    public function tickets(){
        $offices = Office::where('type', '=', 'ticket')->orderBy('priority', 'DESC')->take(3)->get()->translate();
        $posts = Post::where('category_id', '=', 2)->orderBy('id', 'DESC')->take(3)->get()->translate();

        return view('tickets', array_merge(compact('offices', 'posts'), $this->getRightSidebar()));
    }  

    public function category($lang, $slug){
        $category = \App\Category::where('slug', $slug)->first();
        $posts = $category->posts()->latest()->paginate(6);
        $useful = \App\Post::where('category_id', '=', '8')->orderBy('id', 'DESC')->take(3)->get()->translate();
        return view('category', array_merge(compact('posts', 'category', 'useful'), $this->getRightSidebar()));
    }

    public function cars(){
        $offices = Office::where('type', '=', 'auto')->orderBy('priority', 'DESC')->take(3)->get()->translate();
        $posts = Post::where('category_id', '=', 3)->orderBy('id', 'DESC')->take(3)->get()->translate();

        return view('cars', array_merge(compact('offices', 'posts'), $this->getRightSidebar()));
    }  

    public function albums(){
        $useful = \App\Post::where('category_id', '=', '8')
                            ->orderBy('id', 'DESC')->take(3)
                            ->get()->translate();
        $top = \App\Album::orderBy('views', 'desc')->take(8)->get()->translate();
        $latest = \App\Album::take(8)->orderBy('id', 'DESC')->get()->translate();
        $countries = \App\Country::all()->translate();
        return view('albums', compact('top', 'latest', 'useful', 'countries'));
    }

    private function getRightSidebar(){
        $opinions = Post::where('category_id', '=', 5)->orderBy('id', 'DESC')->take(5)->get()->translate();
        $questions = Post::where('category_id', '=', 7)->orderBy('id', 'DESC')->take(5)->get()->translate();
        return compact('opinions', 'questions');
    }
    
    public function test(){
        return request()->all();
    }

    public function country($lang, $slug){
        $country = \App\Country::where('slug', '=', $slug)->first()->translate();
        return view('country', compact('country'));
    }

    public function officecomment($lang, $id){
        $comment = new \App\OfficeComment();
        $comment->user_id = \Auth::user()->id;
        $comment->body = request('body');
        $comment->lang = \App::getLocale();
        $comment->office_id = $id;
        $comment->save();
        return redirect()->back();
    }

    public function album($lang, $slug){
        $album = \App\Album::where('slug', $slug)->first()->translate();
        $opinions = Post::where('category_id', '=', 5)->take(5)->get()->translate();

        return view('album', compact('album', 'opinions'));
    }

    public function travels(){
        $onlyvisas = \App\Tour::whereHas('country', function ($query) {
            $query->where('viza', '=', 'ONLYVISA');
        })->get()->translate();
        $aftervisas = \App\Tour::whereHas('country', function ($query) {
            $query->where('viza', '=', 'AFTERVISA');
        })->get()->translate();
        $novisas = \App\Tour::whereHas('country', function ($query) {
            $query->where('viza', '=', 'NOVISA');
        })->get()->translate();

        $posts = Post::where('category_id', '1')->orderBy('id', 'DESC');
        $posts = request('gid') == '' ? $posts->paginate(4) : $posts->where('author_id', request('gid'))->get();
        $posts = $posts->translate();
        $offices = Office::where('type', 'TOUR')->orderBy('priority', 'DESC')->take(3)->get()->translate();

        return view('travel', array_merge(compact('posts', 'onlyvisas', 'aftervisas', 'novisas', 'offices'), $this->getRightSidebar()));
    }

    public function gids(){
        $gids = \App\Gid::take(4)->get();
        $countries = \App\Country::all()->translate();
        return view('gids', compact('gids', 'countries'));
    }

    public function gid($lang, $id)
    {
        $gid = \App\Gid::find($id)->translate();
        $tours = Post::where('category_id', '1')
                        ->where('author_id', $gid->user->id)
                        ->orderBy('id', 'DESC')
                        ->take(4)
                        ->get()->translate();
        return view('gid', compact('gid', 'tours'));
    }

    public function offices($lang, $type){
        $type = strtoupper($type);
        $offices = \App\Office::where('type', $type)->paginate(8);
        $type = ['AVTO' => 'Avto ijaralar firmalar', 'HOTEL' => 'Mehmonxonalar', 'TICKET' => 'Avia firmalar', 'TOUR' => 'Agentliklar', 'AVIA' => 'Aviakompaniyalar'][$type];
        return view('offices', compact('offices', 'type'));
    }

    public function author($lang, User $user){
        $useful = \App\Post::where('category_id', '=', '8')
                            ->orderBy('id', 'DESC')->take(3)
                            ->get()->translate();
        $posts = Post::where('author_id', $user->id)->paginate(6);
        return view('author',  array_merge(compact('posts', 'user', 'useful'), $this->getRightSidebar()));
    }
}
