<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class AdminController extends Controller
{

	public function smm($id){
		if(!in_array(request()->user()->role_id, [1,4])){
			return;
		}
		$post = \App\Post::find($id);
		// set post fields
		$fields = [
		    'link' => 'https://7days.uz/post/'.$post->slug,
		    'title' => $post->title,
		];

		$ch = curl_init('http://www.example.com');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);

		// execute!
		$response = curl_exec($ch);

		// close the connection, release resources used
		curl_close($ch);

		// do anything you want with your response

		$post->sended = 1;
		$post->save();

		return $response;
	}

	public function post($id){
		$post = \App\Post::find($id);
	}

}
