<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use \App\Post;

class PostController extends BaseController
{
    
    public function show($lang, $slug){	
        $post = Post::where('slug', '=', $slug)->first();
        $post->incrementViews();
        $opinions = Post::where('category_id', '=', '5')->take(4)->get();
        return view('post', compact('post', 'opinions'));
    }
    
}
