// Funksiyalar
String.prototype.filename=function(extension){
    var s= this.replace(/\\/g, '/');
    s= s.substring(s.lastIndexOf('/')+ 1);
    return extension? s.replace(/[?#].+$/, ''): s.split('.')[0];
}

//Boshlaymiz
$(document).ready(function() {
  $('.bodypost.commentsbody img').each(function() {
    var imagepath = $(this).attr('src');
    var imagename = $(this).attr('src').filename();
    var commentlar = '<div class="commentlar '+imagename+'"><img src="'+imagepath+'" alt="Photo" class="photo"><div class="imgbody"><div class="comments" style="visibility: hidden;"><div class="padding"></div></div><div class="nocomment hidden">Hech kim fikr bildirmagan, siz birinchi bo\'ling!</div><div class="chapbar"><div class="combutton"><a href="javascript:void(0)"><img src="boostin/images/ocomment.svg" alt="Open the comments"></a></div><div class="likephoto"><span class="count">90K</span><a href="javascript:void(0)" class="like"><img src="boostin/images/like.svg" alt="Like"></a></div></div><div class="submitcomment"><input type="text" placeholder="Fikr bildirish"><button class="submitcom"><img src="/boostin/images/sendcomment.svg" alt=""></button></div></div></div></div>';

    $(this).replaceWith(commentlar);
    
    
  });
  $('.bodypost.commentsbody .commentlar').each(function() {
       
    var cls = $(this).attr("class").split(' ');
    var c = "." + cls[1];
    var rasmheight = $(c).find("img").outerHeight();
    console.log(rasmheight);
    var rasmwidth = $(c).find("img").outerWidth();
    $(c).find(".imgbody").outerHeight(rasmheight);
    $(c).find(".submitcomment input").outerWidth(rasmwidth - 112);

    function addComment(comment, userName, userPhoto, date) {
      var comment = "<div class=\"cmt\">" + comment + "</div>";
      var userName = "<div class=\"authorname\">" + userName + "</div>";
      var userPhoto = "<div class=\"cavatar\" style=\"background: url('" + userPhoto + "')\"></div>";
      var date = "<div class=\"date\">" + date + "</div>";
      var commentContent = "<div class=\"comment\">" + userPhoto + userName + comment + date + "</div>";
      return commentContent;
    }

    function openComment() {
      $(c).find(".comments").css("opacity", "1");
      $(c).find(".comments").css("visibility", "visible");
      $(c).find(".comments").animate({
        scrollTop: $(c).find(".comments").prop("scrollHeight")
      }, 400);
      $(c).find(".chapbar .combutton a img").attr("src", "/boostin/images/ccomment.svg");
      $(c).find(".chapbar .combutton").addClass("opened");
    };

    function closeComment() {
      $(c).find(".comments").fadeTo(180, 0, function() {
        $(c).find(".comments").css("visibility", "hidden");
      });
      $(c).find(".comments").animate({
        scrollTop: 0
      }, 'fast');
      $(c).find(".chapbar .combutton a img").attr("src", "/boostin/images/ocomment.svg");
      $(c).find(".chapbar .combutton").removeClass("opened");
    }
    $(c).find(".chapbar .combutton a").click(function() {
      if ($(c).find(".chapbar .combutton").is(".opened")) {
        closeComment();
      } else {
        openComment();
      }
    });


    function sComment(thiss) {
      openComment();
      var inputSource = $(thiss).val();
      var commentSub = addComment(inputSource, "Murodxo'ja", "/boostin/images/murod.jpg", "12:21 | 21.01.17")
      $(c).find(".comments .padding").remove();
      $(c).find(".comments").append(commentSub + "<div class=\"padding\"></div>");
      $(thiss).val("");
    }
    $(c).find(".submitcomment button.submitcom").click(function() {
      sComment(".submitcomment input");
    });
    $(c).find(".submitcomment input").keypress(function(e) {
      var commentsheight = $(c).find(".comments").outerHeight();
      if (e.which == 13) {
        sComment(this);
        return false;
      }
    });

  });
  });
  